#!/bin/bash

DBNAME=postgres

echo "#ifndef" __PGCONSTS_H__
echo "#define" __PGCONSTS_H__
echo ""

psql -d $DBNAME -c "select '@', oid, typname from pg_type where typcategory in ('I','B','U','S','N','E','D','V','T','Z') order by oid" | grep "@" | while read L
do
    OID=`echo $L | awk '{print $3}'`
    NAME=`echo $L | awk '{print toupper($5)}' | sed "s/_T//"`
    echo "#define OID_$NAME $OID"
done

echo ""
echo "#endif" // __PGCONSTS_H__
