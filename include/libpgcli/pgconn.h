/*Copyright (c) Brian B.

  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 3 of the License, or (at your option) any later version.
  See the file LICENSE included with this distribution for more
  information.
*/
#ifndef __PGCONN_H__
#define __PGCONN_H__

#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <limits.h>
#include <assert.h>
#include <libex/net.h>
#include <libex/endian.h>
#include "pgerrcodes.h"
#include "pgprov3.h"

#define EPG_PORT 0x80000000
#define EPG_ERRNO 0x40000000
#define EPG_PROTO 0x20000000
#define EPG_HOST 0x0fffffff

#define PG_NRECS(C) C->row_list.len
#define PG_NFLDS(C) C->rowdesc->nflds
#define PG_FLDLEN(C,R,F) C->rows[R]->fields[F].len
#define PG_DATA(C,R,F) C->rows[R]->fields[F].data
#define PG_ISNULL(C,R,F) C->rows[R]->fields[F].len >= 0 ? 0 : 1
#define PG_TYPE(C,F) C->rowdesc->fields[F].oid_field

#define CHECK_BOUNDS(C,R,F) (R >= 0 && R < PG_NRECS(C) && F >= 0 && F < PG_NFLDS(C))

typedef struct {
    char *code;
    char *text;
    char *msg;
    char *detail;
} pgerror_t;

typedef struct pgpool pgpool_t;
typedef struct pgconn pgconn_t;

struct pgconn {
    pgconn_t *prev;
    pgconn_t *next;
    int fd;
    pgmsg_list_t msgs;
    pgmsg_datarow_list_t row_list;
    int nflds;
    uint32_t intr_error;
    pgmsg_datarow_t **rows;
    pgmsg_error_t *error;
    pgmsg_ready_t *ready;
    pgmsg_rowdesc_t *rowdesc;
    pgmsg_cmd_complete_t *complete;
    pgmsg_t *suspended;
    pgmsg_t *emptyquery;
    char *date_style;
    char *client_encoding;
    char *server_encoding;
    char *session_auth;
    char *timezone;
    int is_int_datetimes;
    int is_superuser;
    int major_ver;
    int minor_ver;
    int authok;
    char *dbname;
    int cols;
    int fmt;
    cstr_t *nonce;
    time_t tm_released;
    pgpool_t *pool;
};

#ifndef __GNUC__
void pg_init ();
#endif
void pgconn_init (const char *pg_srv_addr, const char *pg_srv_service);
pgconn_t *pg_connect (const char *url);
#ifdef __GNUC__
const int pg_error (pgconn_t *conn, pgerror_t *e);
#else
int pg_error (pgconn_t *conn, pgerror_t *e);
#endif
static inline char pg_ready (pgconn_t *conn) {
    return conn->ready ? conn->ready->tr : 0;
};
int pg_execsql (pgconn_t *conn, const char *sql, size_t sql_len);
int pg_copyin (pgconn_t *conn, const char *table_name);
int pg_copyin_sendln (pgconn_t *conn, pgfld_t **flds);
int pg_copyin_end (pgconn_t *conn);
static inline int pg_start (pgconn_t* conn) {
    return pg_execsql(conn, CONST_STR_LEN("start transaction"));
};
static inline int pg_commit (pgconn_t *conn) {
    return pg_execsql(conn, CONST_STR_LEN("commit"));
};
static inline int pg_rollback (pgconn_t *conn) {
    return pg_execsql(conn, CONST_STR_LEN("rollback"));
}
int pg_prepareln (pgconn_t *conn, const char *name, size_t name_len,
                  const char *sql, size_t sql_len, int fld_len, pgfld_t **flds);
static inline int pg_preparel (pgconn_t *conn, const char *sql, size_t sql_len, int fld_len, pgfld_t **flds) {
    return pg_prepareln(conn, CONST_STR_NULL, sql, sql_len, fld_len, flds);
}
int pg_preparen (pgconn_t *conn, const char *name, size_t name_len,
                 const char *sql, size_t sql_len, pgfld_t *fld, ...);
int pg_prepare (pgconn_t *conn, const char *sql, size_t sql_len, pgfld_t *fld, ...);
int pg_execln (pgconn_t *conn, const char *portal, size_t portal_len,
               const char *stmt, size_t stmt_len,
               int fld_len, pgfld_t **flds, int res_fmt_len, int *res_fmt, int max_data);
static inline int pg_execl (pgconn_t *conn, int fld_len, pgfld_t **flds, int res_fmt_len, int *res_fmt, int max_data) {
    return pg_execln(conn, CONST_STR_NULL, CONST_STR_NULL, fld_len, flds, res_fmt_len, res_fmt, max_data);
}
int pg_execvn (pgconn_t *conn, const char *portal, size_t portal_len, const char *stmt, size_t stmt_len, int max_data, int fmt_out, pgfld_t *fld, va_list ap);
static inline int pg_execv (pgconn_t *conn, int max_data, int out_fmt, pgfld_t *fld, va_list ap) {
    return pg_execvn(conn, CONST_STR_NULL, CONST_STR_NULL, max_data, out_fmt, fld, ap);
}
int pg_execn (pgconn_t *conn, const char *portal, size_t portal_len, const char *stmt, size_t stmt_len, int max_data, int out_fmt, pgfld_t *fld, ...);
int pg_exec (pgconn_t *conn, int max_data, int out_fmt, pgfld_t *fld, ...);
int pg_nextn (pgconn_t *conn, const char *portal, size_t portal_len, int max_data);
static inline int pg_next (pgconn_t *conn, int max_data) {
    return pg_nextn(conn, CONST_STR_NULL, max_data);
}
static inline char pg_getready (pgconn_t *conn) {
    return conn->ready ? conn->ready->tr : 0;
}
int pg_release (pgconn_t *conn, const char *name, size_t name_len);
void pg_close (pgconn_t *conn);
void pg_disconnect(pgconn_t *conn);

static inline int pg_nrecs (pgconn_t *conn) { return PG_NRECS(conn); };
static inline int pg_nflds (pgconn_t *conn) { return conn->rowdesc ? PG_NFLDS(conn) : 0; };
static inline int pg_isnull (pgconn_t *conn, int row, int col) { return PG_ISNULL(conn, row, col); };

#define pg_getdate(C,R,F) pg_geti32(C,R,F)
#define pg_gettm(C,R,F) pg_geti64(C,R,F)
#ifdef __GNUC__
const strptr_t pg_get (pgconn_t *conn, int row, int col);
#else
strptr_t pg_get (pgconn_t *conn, int row, int col);
#endif
static inline int pg_fldlen (pgconn_t *conn, int row, int col) {
    assert(CHECK_BOUNDS(conn, row, col));
    return PG_FLDLEN(conn, row, col);
}
cstr_t *pg_getstr (pgconn_t *conn, int row, int col);
static inline int16_t pg_geti16 (pgconn_t *conn, int row, int col) {
    assert(CHECK_BOUNDS(conn, row, col));
    return be16toh(*(int16_t*)PG_DATA(conn, row, col));
}

static inline int32_t pg_geti32 (pgconn_t *conn, int row, int col) {
    assert(CHECK_BOUNDS(conn, row, col));
    return be32toh(*(int32_t*)PG_DATA(conn, row, col));
}
static inline int64_t pg_geti64 (pgconn_t *conn, int row, int col) {
    assert(CHECK_BOUNDS(conn, row, col));
    return be64toh(*(int64_t*)PG_DATA(conn, row, col));
}
static inline double pg_getd (pgconn_t *conn, int row, int col) {
    assert(CHECK_BOUNDS(conn, row, col));
    return pg_conv_double(*(double*)PG_DATA(conn, row, col));
}
static inline float pg_getf (pgconn_t *conn, int row, int col) {
    assert(CHECK_BOUNDS(conn, row, col));
    return pg_conv_float(*(float*)PG_DATA(conn, row, col));
}
static inline int8_t pg_getbool (pgconn_t *conn, int row, int col) {
    assert(CHECK_BOUNDS(conn, row, col));
    return *PG_DATA(conn, row, col);
}
uint32_t pg_getx32 (pgconn_t *conn, int row, int col);
pg_intv_t pg_getintv (pgconn_t *conn, int row, int col);
int32_t pg_getinet4 (pgconn_t *conn, int row, int col);
#ifdef HAVE_GMP
static inline void pg_getnum (pgconn_t *conn, int row, int col, mpq_t res, uint16_t *dscale) {
    assert(CHECK_BOUNDS(conn, row, col));
    pg_get_numeric(PG_DATA(conn, row, col), res, dscale);
}
#endif
static inline unsigned char *pg_getuuid (pgconn_t *conn, int row, int col) {
    assert(CHECK_BOUNDS(conn, row, col));
    return PG_DATA(conn, row, col);
}

#endif
