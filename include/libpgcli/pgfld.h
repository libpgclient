/*Copyright (c) Brian B.

  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 3 of the License, or (at your option) any later version.
  See the file LICENSE included with this distribution for more
  information.
*/
#ifndef __PGFLD_H__
#define __PGFLD_H__

#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <netinet/in.h>
#ifdef HAVE_GMP
#include <gmp.h>
#endif
#include <libex/str.h>
#include <libex/time.h>
#include <libex/endian.h>
#include "pgconsts.h"

#define NUMERIC_POS 0x0000
#define NUMERIC_NEG 0x4000
#define NUMERIC_SHORT 0x8000
#define NUMERIC_NAN 0xC000
#define NUMERIC_DSCALE_MASK 0x3FFF

typedef struct in_addr inet_t;
typedef int32_t date_t;
typedef int32_t fsec_t;

typedef struct {
    int32_t len;
    uint32_t bit;
} pg_bit32_t;

typedef struct {
    int64_t time;
    int32_t day;
    int32_t month;
} pg_intv_t;

typedef union {
    int16_t   i2;
    int32_t   i4;
    int64_t   i8;
    float     f4;
    double    f8;
    char      *s;
    int8_t    b;
    int32_t   d;
    tm_t      tm;
    pg_intv_t intv;
#ifdef HAVE_GMP
    mpq_t     q;
#endif
    pg_bit32_t bit32;
    inet_t    sin_addr;
    uint8_t   *u;
} pgval_t;

int timestamp_to_tm (tm_t dt, struct tm *tm);
struct tm *date_to_tm (date_t dt, struct tm *tm);
struct tm *interval_to_tm(pg_intv_t *span, struct tm *tm);
int tm_to_timestamp (struct tm *tm, fsec_t fsec, int *tzp, tm_t *result);
date_t tm_to_date (struct tm *tm);

double pg_conv_double (double d);
float pg_conv_float (float f);

#ifdef HAVE_GMP
void pg_get_numeric (uint8_t *buf, mpq_t res, uint16_t *scale);
str_t *pg_numstr (mpq_ptr x, int base, uint16_t dscale, int float_prec);
#endif

typedef struct pgfld pgfld_t;
struct pgfld {
    int is_null;
    int oid;
    pgval_t data;
    size_t len;
    int fmt;
    int16_t dscale;
};
typedef pgfld_t* pgfld_ptr_t;

#define PG_DEF_MPZ { ._mp_alloc = 0, ._mp_size = 0, ._mp_d = NULL  }
#define PG_DEF_MPQ { ._mp_num = PG_DEF_MPZ, ._mp_den = PG_DEF_MPZ }
#define PG_DEF_NUMERIC_DATA { .q = {PG_DEF_MPQ} }

#define PG_DEF { .is_null = 1, .oid = 0, .fmt = 0 }
#define PG_DEF_SMALL { .is_null = 1, .oid = OID_INT2, .fmt = 1 }
#define PG_DEF_INT { .is_null = 1, .oid = OID_INT4, .fmt = 1 }
#define PG_DEF_BIGINT { .is_null = 1, .oid = OID_INT8, .fmt = 1 }
#define PG_DEF_FLOAT { .is_null = 1, .oid = OID_FLOAT4, .fmt = 1 }
#define PG_DEF_DOUBLE { .is_null = 1, .oid = OID_FLOAT8, .fmt = 1 }
#define PG_DEF_VARCHAR { .is_null = 1, .oid = OID_VARCHAR, .fmt = 0 }
#define PG_DEF_CHAR { .is_null = 1, .oid = OID_CHAR, .fmt = 0 }
#define PG_DEF_BOOL { .is_null = 1, .oid = OID_BOOL, .fmt = 1 }
#define PG_DEF_DATE { .is_null = 1, .oid = OID_DATE, .fmt = 1 }
#define PG_DEF_TIMESTAMP { .is_null = 1, .oid = OID_TIMESTAMP, .fmt = 1 }
#define PG_DEF_UUID { .is_null = 1, .oid = OID_UUID, .fmt = 1 }
#define PG_DEF_BYTEA { .is_null = 1, .oid = OID_BYTEA, .fmt = 1 }
#define PG_DEF_BIT32 { .is_null = 1, .oid = OID_BIT, .fmt = 1 }
#define PG_DEF_MONEY { .is_null = 1, .oid = OID_MONEY, .fmt = 1 }
#define PG_DEF_TEXT { .is_null = 1, .oid = OID_TEXT, .fmt = 0 }

#define PG_SET(X,V,L) X.is_null = 0; X.data.s = V, X.len = 0 == L ? strlen(V) : L
#define PG_SET_NULL(X) X.is_null = 1
#define PG_SET_SMALL(X,V) X.is_null = 0, X.data.i2 = htobe16(V), X.len = sizeof(int16_t)
#define PG_SET_INT(X,V) X.is_null = 0; X.data.i4 = htobe32(V), X.len = sizeof(int32_t)
#define PG_SET_BIGINT(X,V) X.is_null = 0; X.data.i8 = htobe64(V), X.len = sizeof(int64_t)
#define PG_SET_FLOAT(X,V) X.is_null = 0; X.data.f4 = pg_conv_float(V), X.len = sizeof(float)
#define PG_SET_DOUBLE(X,V) X.is_null = 0; X.data.f8 = pg_conv_double(V), X.len = sizeof(double)
#define PG_SET_VARCHAR(X,V,L) X.is_null = 0; X.data.s = V, X.len = 0 == L ? strlen(V) : L
#define PG_SET_CHAR(X,V,L) X.is_null = 0; X.data.s = V, X.len = 0 == L ? strlen(V) : L
#define PG_SET_BOOL(X,V) X.is_null = 0; X.data.b = V ? 1 : 0, X.len = sizeof(int8_t)
#define PG_SET_DATE(X,V) X.is_null = 0; X.data.d = htobe32(V), X.len = sizeof(date_t)
#define PG_SET_TIMESTAMP(X,V) X.is_null = 0; X.data.tm = htobe64(V), X.len = sizeof(tm_t)
#define PG_SET_BYTEA(X,V,L) X.is_null = 0; X.data.s = V; X.len = L
#define PG_SET_UUID(X,V) X.is_null = 0; X.data.u = V, X.len = sizeof(uuid_t)
#define PG_SET_BIT32(X,V) X.is_null = 0, X.data.bit32.bit = htobe32(V), X.data.bit32.len = htobe32(32), X.len = sizeof(pg_bit32_t)
#define PG_SET_TEXT(X,V,L) X.is_null = 0; X.data.s = V, X.len = 0 == L ? strlen(V) : L
#define PG_SET_MONEY(X,V) PG_SET_BIGINT(X,V)

#define PG_INIT(V,L) { .is_null = 0, .oid = 0, .fmt = 0, .data.s = V, .len = 0 == L ? strlen(V) : L }
#define PG_INIT_NULL { .is_null = 1, .oid = 0, .fmt = 0 }
#define PG_INIT_SMALL(V) { .is_null = 0, .oid = OID_INT2, .fmt = 1, .data.i2 = htobe16(V), .len = sizeof(int16_t) }
#define PG_INIT_INT(V) { .is_null = 0, .oid = OID_INT4, .fmt = 1, .data.i4 = htobe32(V), .len = sizeof(int32_t) }
#define PG_INIT_BIGINT(V) { .is_null = 0, .oid = OID_INT8, .fmt = 1, .data.i8 = htobe64(V), .len = sizeof(int64_t) }
#define PG_INIT_FLOAT(V) { .is_null = 0, .oid = OID_FLOAT4, .fmt = 1, .data.f4 = pg_conv_float(V), .len = sizeof(float) }
#define PG_INIT_DOUBLE(V) { .is_null = 0, .oid = OID_FLOAT8, .fmt = 1, .data.f8 = pg_conv_double(V), .len = sizeof(double) }
#define PG_INIT_VARCHAR(V,L) { .is_null = 0, .oid = OID_VARCHAR, .fmt = 0, .data.s = V, .len = 0 == L ? strlen(V) : L }
#define PG_INIT_CHAR(V,L) { .is_null = 0, .oid = OID_CHAR, .fmt = 0, .data.s = V, .len = 0 == L ? strlen(V) : L }
#define PG_INIT_BOOL(V) { .is_null = 0, .oid = OID_BOOL, .fmt = 1, .data.b = V ? 1 : 0, .len = sizeof(int8_t) }
#define PG_INIT_DATE(V), { .is_null = 0, .oid = OID_DATE, .fmt = 1, .data.s = htobe32(V), .len = sizeof(date_t) }
#define PG_INIT_TIMESTAMP(V) { .is_null = 0, .oid = OID_TIMESTAMP, .fmt = 1, .data.tm = htobe64(V), .len = sizeof(int64_t) }
#define PG_INIT_BYTEA(V) { .is_null = 0, .oid = OID_BYTEA, .fmt = 1, .data.s = V, .len = L }
#define PG_INIT_UUID(V) { .is_null = 0, .oid = OID_UUID, .fmt = 1, .data.u = V, .len = sizeof(uuid_t) }
#define PG_INIT_BIT32(V) { .is_null = 0, .oid = OID_BIT, .fmt = 1, .data.bit32.bit = htobe32(V), .data.bit32.len = htobe32(32), .len = sizeof(pg_bit32_t) }
#define PG_INIT_TEXT(V,L) { .is_null = 0, .oid = OID_TEXT, .fmt = 0, .data.s = V, .len = 0 == L ? strlen(V) : L }
#define PG_INIT_MONEY(V) { .is_null = 0, .oid = OID_MONEY, .fmt = 1, .data.i8 = htobe64(V), .len = sizeof(int64_t) }

#endif
