/*Copyright (c) Brian B.

  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 3 of the License, or (at your option) any later version.
  See the file LICENSE included with this distribution for more
  information.
*/
#include "hmac.h"
#include "libpgcli/pgconn.h"

#define PG_STACKSIZE 1024 * 8
#define PG_DEFAULT_PORT 5432

static uint32_t pg_addr = 0;
static unsigned short pg_port = 0;

//******************************************************************************
// Init
//******************************************************************************
#ifdef __GNUC__
__attribute ((constructor))
static void pg_init () {
#else
void pg_init () {
#endif
    struct in_addr addr;
    inet_aton("127.0.0.1", &addr);
    pg_addr = addr.s_addr;
    pg_port = htons(PG_DEFAULT_PORT);
}

#ifdef __GNUC__
__attribute__ ((destructor))
static void _pg_done () {
}
#endif

static int _pg_atoport (const char *service) {
    struct servent *servent = getservbyname(service, "tcp");
    int port;
    char *tail;
    if (servent)
        return servent->s_port;
    port = strtol(service, &tail, 0);
    if ('\0' == *tail || port < 1 || port > USHRT_MAX)
        return -1;
    return htons(port);
}

void pgconn_init (const char *pg_srv_addr, const char *pg_srv_service) {
    struct in_addr addr;
    if (pg_srv_service)
        pg_port = _pg_atoport(pg_srv_service);
    if (pg_srv_addr)
        inet_aton(pg_srv_addr, &addr);
    pg_addr = addr.s_addr;
}

//******************************************************************************
// Data
//******************************************************************************
// Get
//******************************************************************************
#ifdef __GNUC__
const strptr_t pg_get (pgconn_t *conn, int row, int col) {
#else
strptr_t pg_get (pgconn_t *conn, int row, int col) {
#endif
    strptr_t res = { .ptr = NULL, .len = 0 };
    assert(CHECK_BOUNDS(conn, row, col));
    res.ptr = (char*)PG_DATA(conn, row, col);
    res.len = PG_FLDLEN(conn, row, col);
    return res;
};

cstr_t *pg_getstr (pgconn_t *conn, int row, int col) {
    strptr_t s = pg_get(conn, row, col);
    cstr_t *res = NULL;
    if (s.len > 0)
        res = mkcstr(s.ptr, s.len);
    return res;
}

uint32_t pg_getx32 (pgconn_t *conn, int row, int col) {
    assert(CHECK_BOUNDS(conn, row, col));
    pg_bit32_t *res = (pg_bit32_t*)PG_DATA(conn, row, col);
    return be32toh(res->bit);
}

pg_intv_t pg_getintv (pgconn_t *conn, int row, int col) {
    pg_intv_t *i, res = { .time = 0, .day = 0, .month = 0 };
    assert(CHECK_BOUNDS(conn, row, col));
    i = (pg_intv_t*)PG_DATA(conn, row, col),
    res.time = be64toh(i->time);
    res.day = be32toh(i->day);
    res.month = be32toh(i->month);
    return res;
}

typedef union {
    unsigned long l;
    unsigned char a [4];
} pg_inet4_t;
int32_t pg_getinet4 (pgconn_t *conn, int row, int col) {
    uint8_t *v = PG_DATA(conn, row, col);
    if (2 == v[0] && 32 == v[1] && 4 == *(unsigned short*)(v+2)) {
        pg_inet4_t i;
        i.a[0] = v[7];
        i.a[1] = v[6];
        i.a[2] = v[5];
        i.a[3] = v[4];
        return be32toh(i.l);
    }
    return 0;
}

//******************************************************************************
// 
//******************************************************************************
#ifdef __GNUC__
const int pg_error (pgconn_t *conn, pgerror_t *e) {
#else
int pg_error (pgconn_t *conn, pgerror_t *e) {
#endif
    e->code = NULL;
    e->text = NULL;
    e->msg = NULL;
    if ((conn->intr_error & EPG_PORT)) {
        e->code = STR(EPG_PORT);
        e->text = "ERROR";
        e->msg = "Illegal port";
        return -1;
    }
    if ((conn->intr_error & EPG_ERRNO)) {
        e->code = STR(EPG_ERRNO);
        e->text = "ERROR";
        e->msg = strerror(conn->intr_error & EPG_HOST);
        return -1;
    }
    if ((conn->intr_error & EPG_PROTO)) {
        e->code = STR(EPG_PROTO);
        e->text = "ERROR";
        e->msg = "Protocol error";
        return -1;
    }
    switch ((conn->intr_error & EPG_HOST)) {
        case HOST_NOT_FOUND:
            e->code = STR(HOST_NOT_FOUND);
            e->text = "ERROR";
            e->msg = "The specified host is unknown.";
            return -1;
        case NO_DATA:
            e->code = STR(NO_DATA);
            e->text = "ERROR";
            e->msg = "The  requested  name  is  valid  but  does not have an IP address.  Another type of request to the name server for this domain may return an answer.";
            return -1;
        case NO_RECOVERY:
            e->code = STR(NO_RECOVERY);
            e->text = "ERROR";
            e->msg = "A nonrecoverable name server error occurred.";
            return -1;
        case TRY_AGAIN:
            e->code = STR(TRY_AGAIN);
            e->text = "ERROR";
            e->msg = "A temporary error occurred on an authoritative name server.  Try again later.";
            return -1;
    }
    if (conn->error) {
        e->code = conn->error->code;
        e->text = conn->error->text;
        e->msg = conn->error->message;
        e->detail = conn->error->detail;
        return -1;
    }
    e->code = STR(ERRCODE_SUCCESSFUL_COMPLETION);
    e->text = "SUCCESS";
    e->msg = "Success";
    return 0;
}

static void _pg_startup (pgconn_t *conn, const char *conn_info, conninfo_t *cinfo) {
    pgmsg_t *msg = pgmsg_create_startup_params(conn_info);
    if (-1 == pgmsg_send(conn->fd, msg))
        goto done;
    free(msg);
    msg = NULL;
    while (!conn->ready && !conn->error && 0 == pgmsg_recv(conn->fd, &msg)) {
        pgmsg_resp_t *resp = NULL;
        switch (msg->body.type) {
            case PG_READY:
                pgmsg_list_add(&conn->msgs, msg);
                resp = pgmsg_parse(msg);
                conn->ready = &resp->msg_ready;
                msg = NULL;
                break;
            case PG_ERROR:
                pgmsg_list_add(&conn->msgs, msg);
                resp = pgmsg_parse(msg);
                conn->error = &resp->msg_error;
                msg = NULL;
                break;
            case PG_PARAMSTATUS:
                resp = pgmsg_parse(msg);
                if (0 == strcmp(resp->msg_param_status.name, "integer_datetimes"))
                    conn->is_int_datetimes = 0 == strcmp(resp->msg_param_status.value, "on") ? 1 : 0;
                else
                if (0 == strcmp(resp->msg_param_status.name, "is_superuser"))
                    conn->is_superuser = 0 == strcmp(resp->msg_param_status.value, "on") ? 1 : 0;
                else
                if (0 == strcmp(resp->msg_param_status.name, "server_version")) {
                    char *ver = strdup(resp->msg_param_status.value),
                         *p = strchr(ver, '.');
                    if (p) {
                        *p++ = '\0';
                        conn->minor_ver = strtol(p, NULL, 0);
                    }
                    conn->major_ver = strtol(ver, NULL, 0);
                    free(ver);
                } else
                if (0 == strcmp(resp->msg_param_status.name, "client_encoding"))
                    conn->client_encoding = strdup(resp->msg_param_status.value);
                else
                if (0 == strcmp(resp->msg_param_status.name, "server_encoding"))
                    conn->server_encoding = strdup(resp->msg_param_status.value);
                else
                if (0 == strcmp(resp->msg_param_status.name, "session_authorization"))
                    conn->session_auth = strdup(resp->msg_param_status.value);
                else
                if (0 == strcmp(resp->msg_param_status.name, "DateStyle"))
                    conn->date_style = strdup(resp->msg_param_status.value);
                else
                if (0 == strcmp(resp->msg_param_status.name, "TimeZone"))
                    conn->timezone = strdup(resp->msg_param_status.value);
                if (resp) free(resp);
                resp = NULL;
                free(msg);
                msg = NULL;
                break;
            case PG_AUTHOK:
                resp = pgmsg_parse(msg);
                if (!resp) continue;
                conn->authok = resp->msg_auth.success;
                switch (conn->authok) {
                    case PG_REQMD5:
                    case PG_REQPASS:
                        memcpy(cinfo->salt, resp->msg_auth.kind.md5_auth, sizeof(uint8_t) * 4);
                        free(resp);
                        free(msg);
                        msg = pgmsg_create_pass(conn->authok, cinfo->salt, sizeof(uint8_t) * 4, cinfo->user, cinfo->pass);
                        pgmsg_send(conn->fd, msg);
                        free(msg);
                        break;
                    case PG_REQSASL:
                        free(msg);
                        msg = pgmsg_create_sasl_init(cinfo);
                        pgmsg_send(conn->fd, msg);
                        break;
                    case PG_SASLCON:
                        free(msg);
                        msg = pgmsg_create_sasl_fin(resp, cinfo);
                        pgmsg_send(conn->fd, msg);
                        break;
                    case PG_OK:
                    case PG_SASLCOMP:
                        break;
                    default:
                        conn->intr_error = EPG_PROTO;
                        goto done;
                }
                free(msg);
                free(resp);
                msg = NULL;
                break;
            default:
                free(msg);
                msg = NULL;
                break;
        }
    }
done:
    if (msg)
        free(msg);
}

static void *_parse_param (void *data, const char *begin, const char *end, unsigned int *flags) {
    conninfo_t *cinfo = (conninfo_t*)data;
    const char *p = begin, *p1;
    while (p < end && '=' != *p) ++p;
    if (p == end)
        return data;
    p1 = p;
    while (p1 > begin && isspace(*(p1 - 1))) --p1;
    if (p == p1 - 1)
        return data;
    ++p;
    while (p < end && isspace(*p)) ++p;
    if (p == end)
        return data;
    if (0 == strncmp(begin, "host", (uintptr_t)p1 - (uintptr_t)begin)) {
        char *addr = strndup(p, (uintptr_t)end - (uintptr_t)p);
        struct in_addr in_addr;
        memset(&in_addr, 0, sizeof in_addr);
        if (0 == atoaddr(addr, (struct in_addr*)&in_addr))
            cinfo->in_addr.sin_addr = in_addr;
        free(addr);
    } else
    if (0 == strncmp(begin, "port", (uintptr_t)p1 - (uintptr_t)begin)) {
        char *port_s = strndup(p, (uintptr_t)end - (uintptr_t)p), *tail;
        int port = strtol(port_s, &tail, 0);
        if ('\0' == *tail && ERANGE != errno)
            cinfo->in_addr.sin_port = htons(port);
        else
            *flags |= EPG_PORT;
        free(port_s);
    } else
    if (0 == strncmp(begin, "user", (uintptr_t)p1 - (uintptr_t)begin)) {
        if (cinfo->user)
            free(cinfo->user);
        cinfo->user = strndup(p, (uintptr_t)end - (uintptr_t)p);
    } else
    if (0 == strncmp(begin, "password", (uintptr_t)p1 - (uintptr_t)begin)) {
        if (cinfo->pass)
            free(cinfo->pass);
        cinfo->pass = strndup(p, (uintptr_t)end - (uintptr_t)p);
    }
    return data;
}

static str_t *_parse_url (pgconn_t *conn, const char *url) {
    char *str = strdup(url);
    str_t *conninfo = stralloc(64, 64);
    char *s = strstr(str, "://"), *e, *p,
         *user = NULL,
         *password = NULL,
         *host = NULL,
         *port = NULL,
         *dbname = NULL;
    if (!s) {
        free(str);
        return conninfo;
    }
    s += 3;
    if (!*s) {
        free(str);
        return conninfo;
    }
    if ((e = strchr(s, '/')))
        *e = '\0';
    if ((p = strchr(s, '@'))) {
        char *q;
        *p = '\0';
        user = s;
        if ((q = strchr(s, ':'))) {
            *q = '\0';
            password = ++q;
        }
        s = p + 1;
    }
    host = s;
    if ((p = strchr(s, ':'))) {
        *p = '\0';
        port = p + 1;
    }
    if (host) {
        strnadd(&conninfo, CONST_STR_LEN("host="));
        strnadd(&conninfo, host, strlen(host));
    }
    if (port) {
        if (conninfo->len > 0)
            strnadd(&conninfo, CONST_STR_LEN(" "));
        strnadd(&conninfo, CONST_STR_LEN("port="));
        strnadd(&conninfo, port, strlen(port));
    }
    if (user) {
        if (conninfo->len > 0)
            strnadd(&conninfo, CONST_STR_LEN(" "));
        strnadd(&conninfo, CONST_STR_LEN("user="));
        strnadd(&conninfo, user, strlen(user));
    }
    if (password) {
        if (conninfo->len > 0)
            strnadd(&conninfo, CONST_STR_LEN(" "));
        strnadd(&conninfo, CONST_STR_LEN("password="));
        strnadd(&conninfo, password, strlen(password));
    }
    if (!e || !*(++e)) {
        free(str);
        return conninfo;
    }
    s = e;
    if ((e = strchr(s, '?')))
        *e = '\0';
    dbname = s;
    if (conn->dbname) free(conn->dbname);
    conn->dbname = strdup(dbname);
    if (conninfo->len > 0) {
        if (conninfo->len > 0)
            strnadd(&conninfo, CONST_STR_LEN(" "));
        strnadd(&conninfo, CONST_STR_LEN("dbname="));
        strnadd(&conninfo, dbname, strlen(dbname));
    }
    if (!e || !*(++e)) {
        free(str);
        return conninfo;
    }
    s = e;
    str_t *params = strsplit(s, strlen(s), '&');
    strptr_t entry = { .ptr = NULL, .len = 0 };
    while (-1 != strnext(params, &entry)) {
        if ((p = strnchr(entry.ptr, '=', entry.len))) {
            if (conninfo->len > 0)
                strnadd(&conninfo, CONST_STR_LEN(" "));
            strnadd(&conninfo, entry.ptr, entry.len);
        }
    }
    free(params);
    free(str);
    return conninfo;
}

pgconn_t *pg_connect (const char *url) {
    pgconn_t *conn;
    conninfo_t cinfo = { .in_addr = {
                         .sin_family = AF_INET,
                         .sin_port = pg_port,
                         .sin_addr.s_addr = pg_addr },
                         .user = NULL, .pass = NULL,
                         .srv_scram_msg = NULL, .fmsg_bare = NULL,
                         .fmsg_srv = NULL, .fmsg_wproof = NULL,
                         .nonce = NULL };
    int fd;
    uint32_t flags = 0;
    conn = calloc(1, sizeof(pgconn_t));
    str_t *conn_info = _parse_url(conn, url);
    parse_conninfo((void*)&cinfo, conn_info->ptr, _parse_param, &flags);
    if (0 != (conn->intr_error = flags)) {
        free(conn_info);
        return conn;
    }
    fd = socket(AF_INET, SOCK_STREAM, 0);
    if (-1 == connect(fd, (struct sockaddr*)&cinfo.in_addr, sizeof cinfo.in_addr)) {
        free(conn_info);
        close(fd);
        conn->intr_error = EPG_ERRNO | errno;
        return conn;
    }
    conn->fd = fd;
    _pg_startup(conn, conn_info->ptr, &cinfo);
    if (cinfo.user)
        free(cinfo.user);
    if (cinfo.pass)
        free(cinfo.pass);
    if (cinfo.srv_scram_msg)
        free(cinfo.srv_scram_msg);
    if (cinfo.fmsg_bare)
        free(cinfo.fmsg_bare);
    if (cinfo.fmsg_srv)
        free(cinfo.fmsg_srv);
    if (cinfo.fmsg_wproof)
        free(cinfo.fmsg_wproof);
    if (cinfo.nonce)
        free(cinfo.nonce);
    free(conn_info);
    return conn;
}

static void _pg_close (pgconn_t *conn) {
    if (conn->rows) {
        free(conn->rows);
        conn->rows = NULL;
    }
    if (conn->ready) {
        free(conn->ready);
        conn->ready = NULL;
    }
    if (conn->error) {
        free(conn->error);
        conn->error = NULL;
    }
    if (conn->rowdesc) {
        free(conn->rowdesc);
        conn->rowdesc = NULL;
    }
    if (conn->complete) {
        free(conn->complete);
        conn->complete = NULL;
    }
    conn->suspended = NULL;
    pgmsg_datarow_list_clear(&conn->row_list);
    pgmsg_list_clear(&conn->msgs);
    conn->nflds = 0;
}

void pg_close (pgconn_t *conn) {
    _pg_close(conn);
}

void pg_disconnect (pgconn_t *conn) {
    pgmsg_t *msg = pgmsg_create(PG_TERM);
    pgmsg_send(conn->fd, msg);
    free(msg);
    _pg_close(conn);
    if (conn->date_style)
        free(conn->date_style);
    if (conn->client_encoding)
        free(conn->client_encoding);
    if (conn->server_encoding)
        free(conn->server_encoding);
    if (conn->session_auth)
        free(conn->session_auth);
    if (conn->timezone)
        free(conn->timezone);
    if (conn->dbname)
        free(conn->dbname);
    if (conn->nonce)
        free(conn->nonce);
    pgmsg_datarow_list_clear(&conn->row_list);
    pgmsg_list_clear(&conn->msgs);
    if (conn->fd > 0)
        close(conn->fd);
    free(conn);
}

static void _set_rows (pgconn_t *conn) {
    size_t i = 0;
    pgmsg_datarow_list_t *list = &conn->row_list;
    pgmsg_datarow_item_t *item = list->head;
    if (item) {
        conn->rows = malloc(conn->row_list.len * sizeof(void*));
        do {
            conn->rows[i++] = item->datarow;
            item = item->next;
        } while (item != list->head);
    }
}

static void _wait_ready (pgconn_t *conn) {
    pgmsg_t *msg;
    int is_ready = 0;
    while (!is_ready && 0 == pgmsg_recv(conn->fd, &msg)) {
        is_ready = PG_READY == msg->body.type;
        free(msg);
    }
}

static int _simple_exec (pgconn_t *conn) {
    int rc = 0;
    pgmsg_t *msg;
    while (0 == pgmsg_recv(conn->fd, &msg)) {
        pgmsg_resp_t *resp;
        switch (msg->body.type) {
            case PG_READY:
                pgmsg_list_add(&conn->msgs, msg);
                resp = pgmsg_parse(msg);
                conn->ready = &resp->msg_ready;
                if (conn->row_list.len > 0)
                    _set_rows(conn);
                goto done;
            case PG_ERROR:
                pgmsg_list_add(&conn->msgs, msg);
                resp = pgmsg_parse(msg);
                conn->error = &resp->msg_error;
                rc = -1;
                _wait_ready(conn);
                goto done;
            case PG_ROWDESC:
                pgmsg_list_add(&conn->msgs, msg);
                resp = pgmsg_parse(msg);
                conn->rowdesc = &resp->msg_rowdesc;
                conn->nflds = conn->rowdesc->nflds;
                break;
            case PG_CMDCOMPLETE:
                pgmsg_list_add(&conn->msgs, msg);
                resp = pgmsg_parse(msg);
                conn->complete = &resp->msg_complete;
                break;
            case PG_DATAROW:
                pgmsg_list_add(&conn->msgs, msg);
                resp = pgmsg_parse(msg);
                pgmsg_datarow_list_add(&conn->row_list, &resp->msg_datarow);
                break;
            case PG_NODATA:
                free(msg);
                conn->nflds = 0;
                break;
            case PG_COPYIN:
                pgmsg_list_add(&conn->msgs, msg);
                resp = pgmsg_parse(msg);
                conn->cols = resp->msg_copyin.cols;
                conn->fmt = resp->msg_copyin.fmt;
                free(resp);
                goto done;
            default:
                free(msg);
                break;
        }
    }
done:
    return rc;
}

static int _exec (pgconn_t *conn) {
    int rc = 0;
    pgmsg_t *msg;
    _pg_close(conn);
    while (0 == pgmsg_recv(conn->fd, &msg)) {
        pgmsg_resp_t *resp;
        switch (msg->body.type) {
            case PG_READY:
                pgmsg_list_add(&conn->msgs, msg);
                resp = pgmsg_parse(msg);
                conn->ready = &resp->msg_ready;
                if (conn->row_list.len > 0)
                    _set_rows(conn);
                goto done;
            case PG_PARSECOMPLETE:
                free(msg);
                break;
            case PG_BINDCOMPLETE:
                free(msg);
                break;
            case PG_ROWDESC:
                pgmsg_list_add(&conn->msgs, msg);
                resp = pgmsg_parse(msg);
                conn->rowdesc = &resp->msg_rowdesc;
                conn->nflds = conn->rowdesc->nflds;
                break;
            case PG_PARAMDESC:
                free(msg);
                if (0 == pgmsg_recv(conn->fd, &msg)) {
                    switch (msg->body.type) {
                        case PG_ERROR:
                            pgmsg_list_add(&conn->msgs, msg);
                            resp = pgmsg_parse(msg);
                            conn->error = &resp->msg_error;
                            rc = -1;
                            _wait_ready(conn);
                            break;
                        case PG_ROWDESC:
                            pgmsg_list_add(&conn->msgs, msg);
                            resp = pgmsg_parse(msg);
                            conn->rowdesc = &resp->msg_rowdesc;
                            conn->nflds = conn->rowdesc->nflds;
                            break;
                        case PG_NODATA:
                            free(msg);
                            conn->nflds = 0;
                            break;
                        default:
                            free(msg);
                            break;
                    }
                }
            // ins
            case PG_ERROR:
                pgmsg_list_add(&conn->msgs, msg);
                resp = pgmsg_parse(msg);
                conn->error = &resp->msg_error;
                rc = -1;
                _wait_ready(conn);
                goto done;
            case PG_CMDCOMPLETE:
                pgmsg_list_add(&conn->msgs, msg);
                resp = pgmsg_parse(msg);
                conn->complete = &resp->msg_complete;
                break;
            case PG_DATAROW:
                pgmsg_list_add(&conn->msgs, msg);
                resp = pgmsg_parse(msg);
                pgmsg_datarow_list_add(&conn->row_list, &resp->msg_datarow);
                break;
            case PG_NODATA:
                free(msg);
                conn->nflds = 0;
                break;
            case PG_PORTALSUSPENDED:
                pgmsg_list_add(&conn->msgs, msg);
                conn->suspended = msg;
                break;
            case PG_EMPTYQUERY:
                pgmsg_list_add(&conn->msgs, msg);
                conn->emptyquery = msg;
                break;
            default:
                free(msg);
                break;
        }
    }
done:
    return rc;
}

int pg_execsql (pgconn_t *conn, const char *sql, size_t sql_len) {
    int rc;
    pgmsg_t *msg;
    _pg_close(conn);
    if (0 == sql_len)
        sql_len = strlen(sql);
    msg = pgmsg_create_simple_query(sql, sql_len);
    rc = pgmsg_send(conn->fd, msg);
    free(msg);
    if (0 == rc)
        rc = _simple_exec(conn);
    return rc;
}

int pg_copyin (pgconn_t *conn, const char *table_name) {
    cstr_t *sql = cstrfmt("copy %s from stdin", table_name);
    int rc = pg_execsql(conn, sql->ptr, sql->len);
    _pg_close(conn);
    free(sql);
    if (!rc) rc = conn->cols;
    return rc;
}

int pg_copyin_sendln (pgconn_t *conn, pgfld_t **flds) {
    pgmsg_t *msg = pgmsg_copyin_flds(conn->cols, flds);
    if (!msg) return -1;
    _pg_close(conn);
    int rc = pgmsg_send(conn->fd, msg);
    free(msg);
    return rc;
}

int pg_copyin_end (pgconn_t *conn) {
    pgmsg_t *msg = pgmsg_create(PG_COPYEND);
    _pg_close(conn);
    int rc = pgmsg_send(conn->fd, msg);
    free(msg);
    if (0 == rc)
        rc = _simple_exec(conn);
    return rc;
}

static int _pg_sync (pgconn_t *conn) {
    int rc;
    pgmsg_t *msg = pgmsg_create(PG_SYNC);
    rc = pgmsg_send(conn->fd, msg);
    free(msg);
    return rc;
}

int pg_prepareln (pgconn_t *conn, const char *name, size_t name_len, const char *sql, size_t sql_len, int fld_len, pgfld_t **flds) {
    int rc;
    pgmsg_t *msg;
    _pg_close(conn);
    msg = pgmsg_create_parse(name, name_len, sql, sql_len, fld_len, flds);
    rc = pgmsg_send(conn->fd, msg);
    free(msg);
    if (0 == rc && name && 0 == (rc = _pg_sync(conn)))
        rc = _exec(conn);
    return rc;
}

DECLARE_ARRAY(pgfld_array,pgfld_ptr_t)
DECLARE_ARRAY_FUN(pgfld_array,pgfld_ptr_t)

static int pg_preparevn (pgconn_t *conn, const char *name, size_t name_len, const char *sql, size_t sql_len, pgfld_t *fld, va_list ap) {
    int rc;
    pgfld_array_t *flds = create_pgfld_array(16, 16, 0);
    if (fld) {
        add_pgfld_array(&flds, &fld);
        while (NULL != (fld = va_arg(ap, pgfld_ptr_t)))
            add_pgfld_array(&flds, &fld);
    }
    rc = pg_prepareln(conn, name, name_len, sql, sql_len, flds->len, flds->ptr);
    free_pgfld_array(&flds);
    return rc;
}

int pg_preparen (pgconn_t *conn, const char *name, size_t name_len, const char *sql, size_t sql_len, pgfld_t *fld, ...) {
    int rc;
    va_list ap;
    va_start(ap, fld);
    rc = pg_preparevn(conn, name, name_len, sql, sql_len, fld, ap);
    va_end(ap);
    return rc;
}

int pg_prepare (pgconn_t *conn, const char *sql, size_t sql_len, pgfld_t *fld, ...) {
    int rc;
    va_list ap;
    va_start(ap, fld);
    rc = pg_preparevn(conn, CONST_STR_NULL, sql, sql_len, fld, ap);
    va_end(ap);
    return rc;
}

static int _pg_bind (pgconn_t *conn, const char *portal, size_t portal_len, const char *stmt, size_t stmt_len,
                       int fld_len, pgfld_t **flds, int res_fmt_len, int *res_fmt) {
    int rc;
    pgmsg_t *msg;
    _pg_close(conn);
    msg = pgmsg_create_bind(portal, portal_len, stmt, stmt_len, fld_len, flds, res_fmt_len, res_fmt);
    rc = pgmsg_send(conn->fd, msg);
    free(msg);
    return rc;
}

static int _pg_describe (pgconn_t *conn, int8_t op, const char *portal, size_t portal_len) {
    int rc;
    pgmsg_t *msg;
    msg = pgmsg_create_describe(op, portal, portal_len);
    rc = pgmsg_send(conn->fd, msg);
    free(msg);
    return rc;
}

static int _pg_execute (pgconn_t *conn, const char *portal, size_t portal_len, int max_rows) {
    int rc;
    pgmsg_t *msg;
    msg = pgmsg_create_execute(portal, portal_len, max_rows);
    rc = pgmsg_send(conn->fd, msg);
    free(msg);
    if (0 == rc && 0 == (rc = _pg_sync(conn)))
        rc = _exec(conn);
    return rc;
}

int pg_execln (pgconn_t *conn, const char *portal, size_t portal_len, const char *stmt, size_t stmt_len,
               int fld_len, pgfld_t **flds, int res_fmt_len, int *res_fmt, int max_data) {
    int rc;
    if (0 == (rc = _pg_bind(conn, portal, portal_len, stmt, stmt_len, fld_len, flds, res_fmt_len, res_fmt)) &&
        0 == (rc = _pg_describe(conn, PG_PREPARED_PORTAL, portal, portal_len)))
        rc = _pg_execute(conn, portal, portal_len, max_data);
    return rc;
}

int pg_nextn (pgconn_t *conn, const char *portal, size_t portal_len, int max_data) {
    return _pg_execute(conn, portal, portal_len, max_data);
}

int pg_execvn (pgconn_t *conn, const char *portal, size_t portal_len, const char *stmt, size_t stmt_len, int max_data, int out_fmt, pgfld_t *fld, va_list ap) {
    int rc;
    pgfld_array_t *flds = create_pgfld_array(16, 16, 0);
    if (fld) {
        add_pgfld_array(&flds, &fld);
        while (NULL != (fld = va_arg(ap, pgfld_ptr_t)))
            add_pgfld_array(&flds, &fld);
    }
    rc = pg_execln(conn, portal, portal_len, stmt, stmt_len, flds->len, flds->ptr, 1, &out_fmt, max_data);
    free_pgfld_array(&flds);
    return rc;
}

int pg_execn (pgconn_t *conn, const char *portal, size_t portal_len, const char *stmt, size_t stmt_len, int max_data, int out_fmt, pgfld_t *fld, ...) {
    int rc;
    va_list ap;
    va_start(ap, fld);
    rc = pg_execvn(conn, portal, portal_len, stmt, stmt_len, max_data, out_fmt, fld, ap);
    va_end(ap);
    return rc;
}

int pg_exec (pgconn_t *conn, int max_data, int out_fmt, pgfld_t *fld, ...) {
    int rc;
    va_list ap;
    va_start(ap, fld);
    rc = pg_execvn(conn, CONST_STR_NULL, CONST_STR_NULL, max_data, out_fmt, fld, ap);
    va_end(ap);
    return rc;
}

int pg_release (pgconn_t *conn, const char *name, size_t name_len) {
    pgmsg_t *msg = pgmsg_create_close(PG_OPNAME, name, 0 == name_len ? strlen(name) : name_len);
    int rc = pgmsg_send(conn->fd, msg);
    free(msg);
    return rc;
}
