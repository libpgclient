#!/usr/bin/perl
#
# Generate the errcodes.h header from errcodes.txt
# Copyright (c) 2000-2019, PostgreSQL Global Development Group
# patched by Brian B.

use warnings;
use strict;

print "#ifndef __PGERRCODES_H__\n";
print "#define __PGERRCODES_H__\n";

open my $errcodes, '<', $ARGV[0] or die;

while (<$errcodes>)
{
	chomp;

	# Skip comments
	next if /^#/;
	next if /^\s*$/;

	# Emit a comment for each section header
	if (/^Section:(.*)/)
	{
		my $header = $1;
		$header =~ s/^\s+//;
		print "\n/* $header */\n";
		next;
	}

	die "unable to parse errcodes.txt"
	  unless /^([^\s]{5})\s+[EWS]\s+([^\s]+)/;

	(my $sqlstate, my $errcode_macro) = ($1, $2);

	print "#define $errcode_macro \"$sqlstate\"\n";
}

print "\n#endif\n";

close $errcodes;
