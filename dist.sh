#!/bin/sh

VER=`cat libpgcli.pc.in | grep Version | awk '{print $2}'`
DIR=libpgclient-$VER
(rm libpgclient-$VER.tar.bz2 2>/dev/null || true)
mkdir -p $DIR $DIR/include/libpgcli $DIR/src $DIR/util
cp include/libpgcli/* $DIR/include/libpgcli
cp src/* $DIR/src
cp util/* $DIR/util
cp Jamfile $DIR/
cp Jamrules.configure $DIR/
cp LICENSE $DIR/
cp README.md $DIR/
cp libpgcli.pc.in $DIR/
tar cvf libpgclient-$VER.tar $DIR/
bzip2 libpgclient-$VER.tar
rm -fr $DIR/
