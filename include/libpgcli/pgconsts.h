#ifndef __PGCONSTS_H__
#define __PGCONSTS_H__

#define OID_BOOL 16
#define OID_BYTEA 17
#define OID_CHAR 18
#define OID_NAME 19
#define OID_INT8 20
#define OID_INT2 21
#define OID_INT4 23
#define OID_REGPROC 24
#define OID_TEXT 25
#define OID_OID 26
#define OID_TID 27
#define OID_XID 28
#define OID_CID 29
#define OID_JSON 114
#define OID_XML 142
#define OID_PG_NODEREE 194
#define OID_CIDR 650
#define OID_FLOAT4 700
#define OID_FLOAT8 701
#define OID_MACADDR8 774
#define OID_MONEY 790
#define OID_MACADDR 829
#define OID_INET 869
#define OID_ACLITEM 1033
#define OID_BPCHAR 1042
#define OID_VARCHAR 1043
#define OID_DATE 1082
#define OID_TIME 1083
#define OID_TIMESTAMP 1114
#define OID_TIMESTAMPTZ 1184
#define OID_INTERVAL 1186
#define OID_TIMETZ 1266
#define OID_BIT 1560
#define OID_VARBIT 1562
#define OID_NUMERIC 1700
#define OID_REFCURSOR 1790
#define OID_REGPROCEDURE 2202
#define OID_REGOPER 2203
#define OID_REGOPERATOR 2204
#define OID_REGCLASS 2205
#define OID_REGTYPE 2206
#define OID_UUID 2950
#define OID_TXID_SNAPSHOT 2970
#define OID_PG_LSN 3220
#define OID_PG_NDISTINCT 3361
#define OID_PG_DEPENDENCIES 3402
#define OID_TSVECTOR 3614
#define OID_TSQUERY 3615
#define OID_GTSVECTOR 3642
#define OID_REGCONFIG 3734
#define OID_REGDICTIONARY 3769
#define OID_JSONB 3802
#define OID_JSONPATH 4072
#define OID_REGNAMESPACE 4089
#define OID_REGROLE 4096
#define OID_REGCOLLATION 4191
#define OID_PG_BRIN_BLOOM_SUMMARY 4600
#define OID_PG_BRIN_MINMAX_MULTI_SUMMARY 4601
#define OID_PG_MCV_LIST 5017
#define OID_PG_SNAPSHOT 5038
#define OID_XID8 5069
#define OID_CARDINAL_NUMBER 12417
#define OID_CHARACTER_DATA 12420
#define OID_SQL_IDENTIFIER 12422
#define OID_TIME_STAMP 12428
#define OID_YES_OR_NO 12430

#endif // __PGCONSTS_H__
