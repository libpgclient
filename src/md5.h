/*Copyright (c) Brian B.

  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 3 of the License, or (at your option) any later version.
  See the file LICENSE included with this distribution for more
  information.
*/
#ifndef __MD5_H__
#define __MD5_H__

#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <stdlib.h>

typedef struct{
    uint64_t size;
    uint32_t buffer[4];
    uint8_t input[64];
    uint8_t digest[16];
} md5_t;

void md5_init(md5_t *ctx);
void md5_update(md5_t *ctx, uint8_t *input, size_t input_len);
void md5_final(md5_t *ctx);

#endif
