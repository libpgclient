/*Copyright (c) Brian B.

  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 3 of the License, or (at your option) any later version.
  See the file LICENSE included with this distribution for more
  information.
*/
#include <stdio.h>
#include <libex/str.h>
#include "../include/libpgcli/pgconn.h"
#include "linenoise.h"
#include "pgsql_cmd.h"

static void usage (const char *progname) {
    dprintf(STDERR_FILENO, "usage: %s <connection_string> [start_sql]\n", progname);
    exit(1);
}

static int print_error (pgconn_t *conn) {
    int rc;
    pgerror_t e;
    if (-1 == (rc = pg_error(conn, &e)))
        dprintf(STDERR_FILENO, "%s: %s; CODE: %s\n", e.text, e.msg, e.code);
    return rc;
}

static int exec_sql (pgconn_t *conn, const char *sql, size_t sql_len) {
    if (pg_execsql(conn, sql, sql_len)) {
        print_error(conn);
        pg_close(conn);
        return -1;
    }
    if (pg_nflds(conn) > 0)
        print_result(conn);
    if (conn->complete)
        printf("%s\n", conn->complete->tag);
    pg_close(conn);
    return 0;
}

static void intro (pgconn_t *conn) {
    cstr_t *schema = get_current_schema(conn),
           *dbsize = get_dbsize(conn, conn->dbname);
    printf("Server version: %d.%d\n", conn->major_ver, conn->minor_ver);
    printf("Server time zone: %s\n", conn->timezone);
    printf("Database: %s (%s)\n", conn->dbname, dbsize->ptr);
    printf("Current schema: %s\n", schema->ptr);
    free(dbsize);
    free(schema);
}

int main (int argc, const char *argv[]) {
    if (argc < 2 || argc > 3) usage(argv[0]);
    pgconn_t *conn = pg_connect(argv[1]);
    if (0 == print_error(conn) && PG_IDLE == pg_ready(conn)) {
        int is_quit = 0;
        char *homedir = getenv("HOME");
        str_t *hist = NULL;
        if (3 == argc) {
            printf("%s\n", argv[2]);
            exec_sql(conn, argv[2], strlen(argv[2]));
            pg_disconnect(conn);
            return 0;
        }
        if (homedir) {
            hist = strfmt("%s/.pgsql_history", homedir);
            linenoiseHistoryLoadFile(hist->ptr);
        }
        intro(conn);
        while (!is_quit) {
            str_t *sql = NULL;
            char *line;
            while ((line = linenoise(sql ? "# " : "> ", NULL))) {
                if (sql)
                    strnadd(&sql, line, strlen(line));
                else {
                    if (!strcmp(line, "quit") || !strcmp(line, "exit")) {
                        is_quit = 1;
                        break;
                    } else if (!strncmp(line, "show", 4)) {
                        char **str = &line;
                        size_t str_len = strlen(line);
                        show(conn, str, &str_len);
                        continue;
                    } else
                        sql = mkstr(line, strlen(line), 0);
                }
                char *e = sql->ptr + sql->len - 1;
                while (e > sql->ptr && isspace(*e)) --e;
                if (';' == *e) {
                    *e = ' ';
                    break;
                } else
                    strnadd(&sql, CONST_STR_LEN(" "));
            }
            if (!is_quit && sql) {
                if (0 == exec_sql(conn, sql->ptr, sql->len)) {
                    sql->ptr[sql->len-1] = ';';
                    linenoiseHistoryAdd(sql->ptr);
                }
            }
            if (sql) free(sql);
            sql = NULL;
        }
        if (hist) {
            linenoiseHistorySaveFile(hist->ptr);
            free(hist);
        }
    }
    pg_disconnect(conn);
    return 0;
}
