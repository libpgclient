/*Copyright (c) Brian B.

  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 3 of the License, or (at your option) any later version.
  See the file LICENSE included with this distribution for more
  information.

  Portions Copyright (c) 1994, Regents of the University of California

*/
#ifndef __HMAC_H__
#define __HMAC_H__

#include <stdlib.h>
#include <stdint.h>

#define SHA_BLOCK_LEN 64
#define SHA_DIGEST_LEN 32
#define SHA_DIGEST_STRLEN (SHA_DIGEST_LEN * 2 + 1)
#define HMAC_IPAD 0x36
#define HMAC_OPAD 0x5C

typedef struct {
    uint32_t state [8];
    uint64_t bitcount;
    uint8_t buf [SHA_BLOCK_LEN];
} sha_t;
void sha_init (sha_t *ctx);
void sha_update (sha_t *ctx, const uint8_t *data, size_t len);
void sha_final (sha_t *ctx, uint8_t *digest);

typedef struct {
    sha_t hash;
    uint8_t ipad [SHA_BLOCK_LEN];
    uint8_t opad [SHA_BLOCK_LEN];
} hmac_t;
void hmac_init (hmac_t *ctx, uint8_t *key, size_t len);
void hmac_update (hmac_t *ctx, const uint8_t *data, size_t len);
void hmac_final (hmac_t *ctx, uint8_t *dst, size_t len);

#endif
