/*Copyright (c) Brian B.

  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 3 of the License, or (at your option) any later version.
  See the file LICENSE included with this distribution for more
  information.
*/
#include "md5.h"
#include "hmac.h"
#include "libpgcli/pgprov3.h"

#define CHUNKSIZE 32
#define HEADER_SIZE sizeof(char) + sizeof(int32_t)

#define ADD_LIST_ITEM(list,item) \
    if (!list->head) { \
        item->next = item->prev = item; \
        list->head = item; \
    } else { \
        item->next = list->head; \
        item->prev = list->head->prev; \
        list->head->prev = item; \
        item->prev->next = item; \
    } \
    ++list->len;

void pgmsg_list_add (pgmsg_list_t *list, pgmsg_t *msg) {
    ADD_LIST_ITEM(list, msg)
}

void pgmsg_datarow_list_add (pgmsg_datarow_list_t *list, pgmsg_datarow_t *datarow) {
    pgmsg_datarow_item_t *item = calloc(1, sizeof(pgmsg_datarow_item_t));
    item->datarow = datarow;
    ADD_LIST_ITEM(list, item)
}

#define DEL_LIST_ITEM(type,list,item) \
    type *prev = item->prev, *next = item->next; \
    if (prev) prev->next = next; \
    if (next) next->prev = prev; \
    if (item == list->head) list->head = next; \
    if (0 == --list->len) list->head = NULL;

static void _pgmsg_list_del (pgmsg_list_t *list, pgmsg_t *msg) {
    DEL_LIST_ITEM(pgmsg_t,list, msg)
}

static void _pgmsg_datarow_list_del (pgmsg_datarow_list_t *list, pgmsg_datarow_item_t *item) {
    DEL_LIST_ITEM(pgmsg_datarow_item_t,list, item)
    free(item);
}

void pgmsg_list_clear (pgmsg_list_t *list) {
    while (list->head)
        _pgmsg_list_del(list, list->head);
}

void pgmsg_datarow_list_clear (pgmsg_datarow_list_t *list) {
    while (list->head)
        _pgmsg_datarow_list_del(list, list->head);
}

static int _resize (pgmsg_t **msg, size_t len) {
    pgmsg_t *res;
    int nlen = (*msg)->len + len;
    size_t pc_len,
           bufsize = sizeof(pgmsg_t) + (nlen / CHUNKSIZE) * CHUNKSIZE + CHUNKSIZE;
    if (bufsize == (*msg)->bufsize)
        return 0;
    pc_len = (uintptr_t)(*msg)->pc - (uintptr_t)(*msg)->body.ptr;
    if (!(res = realloc(*msg, bufsize)))
        return -1;
    res->bufsize = bufsize;
    res->pc = res->body.ptr + pc_len;
    *msg = res;
    return 0;
}

pgmsg_t *pgmsg_create (char type) {
    size_t bufsize = sizeof(pgmsg_t) + CHUNKSIZE;
    pgmsg_t *msg = malloc(bufsize);
    if (!msg) return NULL;
    msg->bufsize = bufsize;
    msg->len = sizeof(pgmsg_t);
    msg->pc = msg->body.ptr;
    msg->body.type = type;
    msg->body.len = htobe32(sizeof(int32_t));
    return msg;
}

static int _seti8 (pgmsg_t **msg, int8_t x) {
    if (-1 == _resize(msg, sizeof(int8_t)))
        return -1;
    *((int8_t*)(*msg)->pc) = x;
    (*msg)->pc += sizeof(int8_t);
    (*msg)->body.len = htobe32(be32toh((*msg)->body.len) + sizeof(int8_t));
    (*msg)->len += sizeof(int8_t);
    return 0;
}

static int _seti16 (pgmsg_t **msg, int16_t x) {
    if (-1 == _resize(msg, sizeof(int16_t)))
        return -1;
    *((int16_t*)(*msg)->pc) = htobe16(x);
    (*msg)->pc += sizeof(int16_t);
    (*msg)->body.len = htobe32(be32toh((*msg)->body.len) + sizeof(int16_t));
    (*msg)->len += sizeof(int16_t);
    return 0;
}

static int _seti32 (pgmsg_t **msg, int32_t x) {
    if (-1 == _resize(msg, sizeof(int32_t)))
        return -1;
    *((int32_t*)(*msg)->pc) = htobe32(x);
    (*msg)->pc += sizeof(int32_t);
    (*msg)->body.len = htobe32(be32toh((*msg)->body.len) + sizeof(int32_t));
    (*msg)->len += sizeof(int32_t);
    return 0;
}

static int _setstr (pgmsg_t **msg, const char *s, size_t slen) {
    if (0 == slen)
        return 0;
    if (-1 == _resize(msg, slen))
        return -1;
    memcpy((*msg)->pc, s, slen);
    (*msg)->pc += slen;
    (*msg)->body.len = htobe32(be32toh((*msg)->body.len) + slen);
    (*msg)->len += slen;
    return 0;
}

static int8_t _geti8 (pgmsg_t *msg) {
    int8_t r = *msg->pc;
    msg->pc += sizeof(int8_t);
    return r;
}

static int16_t _geti16 (pgmsg_t *msg) {
    int16_t r =  be16toh(*((int16_t*)msg->pc));
    msg->pc += sizeof(int16_t);
    return r;
}

static int32_t _geti32 (pgmsg_t *msg) {
    int32_t r = be32toh(*((int32_t*)msg->pc));
    msg->pc += sizeof(int32_t);
    return r;
}

static const char *_getstr (pgmsg_t *msg) {
    char *p = msg->pc, *e = msg->body.ptr + be32toh(msg->body.len);
    while (*(msg->pc) && msg->pc < e) ++msg->pc;
    if (e == msg->pc)
        return NULL;
    ++msg->pc;
    return p;
}

int pgmsg_set_param (pgmsg_t **msg, const char *name, size_t name_len, const char *value, size_t value_len) {
    if (-1 == _setstr(msg, name, name_len) ||
        -1 == _seti8(msg, 0) ||
        -1 == _setstr(msg, value, value_len) ||
        -1 == _seti8(msg, 0))
        return -1;
    return 0;
}

pgmsg_t *pgmsg_create_startup (const char *user, size_t user_len, const char *database, size_t database_len) {
    pgmsg_t *msg = pgmsg_create('\0');
    _seti16(&msg, PG_MAJOR_VER);
    _seti16(&msg, PG_MINOR_VER);
    pgmsg_set_param(&msg, CONST_STR_LEN("user"), user, user_len);
    pgmsg_set_param(&msg, CONST_STR_LEN("database"), database, database_len);
    return msg;
}

#define PG_USERDEFS 0x00000001
#define PG_DBDEFS 0x00000002
#define PG_ENCDEFS 0x00000004

static inline pgmsg_t *_add_str_param (pgmsg_t *msg, const char *k, size_t lk, const char *v, size_t lv) {
    _setstr(&msg, k, lk);
    _seti8(&msg, 0);
    _setstr(&msg, v, lv);
    _seti8(&msg, 0);
    return msg;
}

static pgmsg_t *_add_param (pgmsg_t *msg, const char *begin, const char *end, uint32_t *flags) {
    const char *p = begin, *p1;
    while (p < end && '=' != *p) ++p;
    if (0 == strncmp(begin, "password", (uintptr_t)p - (uintptr_t)begin))
        return msg;
    if (p == end)
        return msg;
    p1 = p;
    while (p1 > begin && isspace(*(p1 - 1))) --p1;
    if (p == p1 - 1)
        return msg;
    ++p;
    while (p < end && isspace(*p)) ++p;
    if (p == end)
        return msg;
    if (0 == strncmp(begin, "dbname", (uintptr_t)p1 - (uintptr_t)begin)) {
        msg = _add_str_param(msg, CONST_STR_LEN("database"), p, (uintptr_t)end - (uintptr_t)p);
        if (flags)
            *flags |= PG_DBDEFS;
    } else
    if (0 == strncmp(begin, "user", (uintptr_t)p1 - (uintptr_t)begin)) {
        msg = _add_str_param(msg, begin, (uintptr_t)p1 - (uintptr_t)begin, p, (uintptr_t)end - (uintptr_t)p);
        if (flags)
            *flags |= PG_USERDEFS;
    } else
    if (0 != strncmp(begin, "host", (uintptr_t)p1 - (uintptr_t)begin) &&
        0 != strncmp(begin, "port", (uintptr_t)p1 - (uintptr_t)begin))
        msg = _add_str_param(msg, begin, (uintptr_t)p1 - (uintptr_t)begin, p, (uintptr_t)end - (uintptr_t)p);
    return msg;
}

void *parse_conninfo (void *data, const char *conn_info, parse_param_h fn, uint32_t *flags) {
    if (conn_info) {
        const char *p = conn_info;
        while (*p) {
            const char *q;
            while (isspace(*p)) ++p;
            if (!(*p)) break;
            q = p;
            while (*p && !isspace(*p)) ++p;
            data = fn(data, q, p, flags);
        }
    }
    return data;
}

static pgmsg_t *_startup_params (pgmsg_t *msg, const char *conn_info) {
    uint32_t flags = 0;
    msg = (pgmsg_t*)parse_conninfo((void*)msg, conn_info, (parse_param_h)_add_param, &flags);
    if (!(flags & PG_USERDEFS)) {
        char *s = getenv("USER");
        if (s)
            msg = _add_str_param(msg, CONST_STR_LEN("user"), s, strlen(s));
    }
    if (!(flags & PG_DBDEFS)) {
        char *s = getenv("USER");
        if (s)
            msg = _add_str_param(msg, CONST_STR_LEN("database"), s, strlen(s));
    }
    if (!(flags & PG_ENCDEFS)) {
        char *s = getenv("LANG");
        if (s && (s = strchr(s, '.')) && *++s)
            msg = _add_str_param(msg, CONST_STR_LEN("client_encoding"), s, strlen(s));
    }
    _seti8(&msg, 0);
    return msg;
}

pgmsg_t *pgmsg_create_startup_params (const char *conn_info) {
    pgmsg_t *msg = pgmsg_create('\0');
    _seti16(&msg, PG_MAJOR_VER);
    _seti16(&msg, PG_MINOR_VER);
    return _startup_params(msg, conn_info);
}

pgmsg_t *pgmsg_create_simple_query (const char *sql, size_t sql_len) {
    pgmsg_t *msg = pgmsg_create(PG_SIMPLEQUERY);
    _setstr(&msg, sql, sql_len);
    _seti8(&msg, 0);
    return msg;
}

pgmsg_t *pgmsg_create_parse (const char *name, size_t name_len, const char *sql, size_t sql_len, int fld_len, pgfld_t **flds) {
    pgmsg_t *msg = pgmsg_create(PG_PARSE);
    if (name && 0 == name_len)
        name_len = strlen(name);
    if (0 == sql_len)
        sql_len = strlen(sql);
    _setstr(&msg, name, name_len);
    _seti8(&msg, 0);
    _setstr(&msg, sql, sql_len);
    _seti8(&msg, 0);
    _seti16(&msg, fld_len);
    for (int i = 0; i < fld_len; ++i)
        _seti32(&msg, flds[i]->oid);
    return msg;
}

pgmsg_t *pgmsg_create_bind (const char *portal, size_t portal_len, const char *stmt, size_t stmt_len,
                            int fld_len, pgfld_t **flds, int res_fmt_len, int *res_fmt) {
    pgmsg_t *msg = pgmsg_create(PG_BIND);
    _setstr(&msg, portal, portal_len);
    _seti8(&msg, 0);
    _setstr(&msg, stmt, stmt_len);
    _seti8(&msg, 0);
    _seti16(&msg, fld_len);
    for (int i = 0; i < fld_len; ++i)
        _seti16(&msg, flds[i]->fmt);
    _seti16(&msg, fld_len);
    for (int i = 0; i < fld_len; ++i) {
        if (flds[i]->is_null)
            _seti32(&msg, -1);
        else {
            _seti32(&msg, flds[i]->len);
            if (0 == flds[i]->fmt || OID_UUID == flds[i]->oid)
                _setstr(&msg, flds[i]->data.s, flds[i]->len);
            else
                _setstr(&msg, (const char*)&flds[i]->data, flds[i]->len);
        }
    }
    _seti16(&msg, res_fmt_len);
    for (int i = 0; i < res_fmt_len; ++i)
        _seti16(&msg, res_fmt[i]);
    return msg;
}

pgmsg_t *pgmsg_create_describe (uint8_t op, const char *name, size_t name_len) {
    pgmsg_t *msg = pgmsg_create(PG_DESCRIBE);
    _seti8(&msg, op);
    _setstr(&msg, name, name_len);
    _seti8(&msg, 0);
    return msg;
}

pgmsg_t *pgmsg_create_execute (const char *portal, size_t portal_len, int32_t max_rows) {
    pgmsg_t *msg = pgmsg_create(PG_EXECUTE);
    _setstr(&msg, portal, portal_len);
    _seti8(&msg, 0);
    _seti32(&msg, max_rows);
    return msg;
}

pgmsg_t *pgmsg_create_close(char what, const char *str, size_t slen) {
    pgmsg_t *msg = pgmsg_create(PG_CLOSE);
    _seti8(&msg, (uint8_t)what);
    _setstr(&msg, str, slen);
    _seti8(&msg, 0);
    return msg;
}

static void _pg_md5_hash (const void *buff, size_t len, char *out) {
    uint8_t digest [16];
    md5_t ctx;
    md5_init(&ctx);
    while (len > 0) {
        if (len > 512)
            md5_update(&ctx, (uint8_t*)buff, 512);
        else
            md5_update(&ctx, (uint8_t*)buff, len);
        buff += 512;
        len -= 512;
    }
    md5_final(&ctx);
    memcpy(digest, ctx.digest, sizeof digest);
    for (int i = 0; i < 16; ++i)
        snprintf(&(out[i*2]), 16*2, "%02x", (uint8_t)digest[i]);
}

static void _pg_md5_encrypt(const char *passwd, const char *salt, size_t salt_len, char *buf) {
    size_t passwd_len = strlen(passwd);
    char *crypt_buf = malloc(passwd_len + salt_len + 1);
    memcpy(crypt_buf, passwd, passwd_len);
    memcpy(crypt_buf + passwd_len, salt, salt_len);
    strcpy(buf, "md5");
    _pg_md5_hash(crypt_buf, passwd_len + salt_len, buf + 3);
    free(crypt_buf);
}

pgmsg_t *pgmsg_create_pass (int req, const char *salt, size_t salt_len, const char *user, const char *pass) {
    pgmsg_t *msg = pgmsg_create(PG_PASS);
    char *pwd = malloc(2 * (PG_MD5PASS_LEN + 1)),
         *pwd2 = pwd + PG_MD5PASS_LEN + 1,
         *pwd_to_send;
    _pg_md5_encrypt(pass, user, strlen(user), pwd2);
    _pg_md5_encrypt(pwd2 + sizeof("md5")-1, salt, 4, pwd);
    pwd_to_send = pwd;
    switch (req) {
        case PG_REQMD5:
            pwd_to_send = pwd;
            break;
        case PG_REQPASS:
            pwd_to_send = (char*)pass;
    }
    _setstr(&msg, pwd_to_send, strlen(pwd_to_send));
    _seti8(&msg, 0);
    free(pwd);
    return msg;
}

pgmsg_t *pgmsg_create_sasl_init (conninfo_t *cinfo) {
    char raw_nonce [SCRAM_RAW_NONCE_LEN+1];
    strand(raw_nonce, SCRAM_RAW_NONCE_LEN, RAND_ALNUM);
    raw_nonce[SCRAM_RAW_NONCE_LEN] = '\0';
    cinfo->nonce = cstr_b64encode(raw_nonce, SCRAM_RAW_NONCE_LEN);
    str_t *str = strfmt("n,,n=,r=%s", cinfo->nonce->ptr);
    cinfo->fmsg_bare = mkcstr(str->ptr+3, str->len-3);
    pgmsg_t *msg = pgmsg_create(PG_PASS);
    _setstr(&msg, CONST_STR_LEN("SCRAM-SHA-256"));
    _seti8(&msg, 0);
    _seti32(&msg, str->len);
    _setstr(&msg, str->ptr, str->len);
    free(str);
    return msg;
}

static int _parse_scram_final (pgmsg_resp_t *resp, conninfo_t *cinfo) {
    strptr_t entry = { .ptr = NULL, .len = 0 };
    char *data = (char*)resp->msg_auth.kind.sasl_auth.data;
    int len = resp->msg_auth.kind.sasl_auth.len;
    if ('e' == *resp->msg_auth.kind.sasl_auth.data)
        return -1;
    if (!(cinfo->srv_scram_msg = strsplit(data, len, ',')))
        return -1;
    cinfo->fmsg_srv = mkcstr(data, len);
    while (0 == strnext(cinfo->srv_scram_msg, &entry)) {
        if (entry.len < 3 && '=' != entry.ptr[1])
            continue;
        switch (*entry.ptr) {
            case 'r':
                cinfo->r_attr = entry.ptr+2;
                break;
            case 's':
                cinfo->s_attr = entry.ptr+2;
                break;
            case 'i':
                cinfo->i_attr = entry.ptr+2;
                break;
        }
    }
    return cinfo->r_attr && cinfo->s_attr && cinfo->i_attr ? 0 : -1;
}

static void _scram_create_key (uint8_t *salted_password, uint8_t *result) {
    hmac_t ctx;
    hmac_init(&ctx, salted_password, SCRAM_KEY_LEN);
    hmac_update(&ctx, (uint8_t*)"Client Key", sizeof("Client Key")-1);
    hmac_final(&ctx, result, SCRAM_KEY_LEN);
}

char *(*on_pgauth) (const char *prompt, int is_echo);
static struct termios term_settings;
static char *_pg_auth (const char *prompt, const char *def_auth, int is_echo) {
    char *s = NULL;
    size_t len = 0;
    ssize_t ilen;
    struct termios tc;
    if (on_pgauth)
        return on_pgauth(prompt, is_echo);
    printf("%s", prompt);
    if (!is_echo) {
        tcgetattr(0, &term_settings);
        tc = term_settings;
        tc.c_lflag &= ~ECHO;
        tcsetattr(0, TCSANOW, &tc);
    }
    if (-1 == (ilen = getline(&s, &len, stdin))) {
        if (!is_echo)
            tcsetattr(0, TCSANOW, &term_settings);
        return strdup(def_auth);
    }
    if (!is_echo) {
        tcsetattr(0, TCSANOW, &term_settings);
        printf("\n");
    }
    s[ilen-1] = '\0';
    return s;
}

static void _scram_salted_password (conninfo_t *cinfo, cstr_t *salt) {
    hmac_t ctx;
    if (!cinfo->user)
        cinfo->user = _pg_auth("Enter username: ", getenv("USER"), 1);
    if (!cinfo->pass)
        cinfo->pass = _pg_auth("Enter password: ", "", 0);
    int password_len = strlen(cinfo->pass);
    uint32_t one = htobe32(1);
    uint8_t ui_prev [SCRAM_KEY_LEN],
            ui [SCRAM_KEY_LEN];
    int iterations = strtol(cinfo->i_attr, NULL, 0);
    hmac_init(&ctx, (uint8_t*)cinfo->pass, password_len);
    hmac_update(&ctx, (uint8_t*)salt->ptr, strlen(salt->ptr));
    hmac_update(&ctx, (uint8_t*)&one, sizeof(uint32_t));
    hmac_final(&ctx, ui_prev, sizeof(ui_prev));
    memcpy(cinfo->salted_password, ui_prev, SCRAM_KEY_LEN);
    for (int i = 2; i <= iterations; ++i) {
        hmac_init(&ctx, (uint8_t*)cinfo->pass, password_len);
        hmac_update(&ctx, ui_prev, SCRAM_KEY_LEN);
        hmac_final(&ctx, ui, SCRAM_KEY_LEN);
        for (int j = 0; j < SCRAM_KEY_LEN; ++j)
            cinfo->salted_password[j] ^= ui[j];
        memcpy(ui_prev, ui, SCRAM_KEY_LEN);
    }
}

static void _scram_h (uint8_t *in, int len, uint8_t *result) {
    sha_t ctx;
    memset(&ctx, 0, sizeof ctx);
    sha_init(&ctx);
    sha_update(&ctx, in, len);
    sha_final(&ctx, result);
}

static void _calc_scram_proof (conninfo_t *cinfo, uint8_t *result) {
    hmac_t ctx;
    uint8_t client_key [SCRAM_KEY_LEN],
            stored_key [SCRAM_KEY_LEN],
            clsign_key [SCRAM_KEY_LEN];
    cstr_t *salt = cstr_b64decode(cinfo->s_attr, strlen(cinfo->s_attr));
    _scram_salted_password(cinfo, salt);
    free(salt);
    _scram_create_key((uint8_t*)cinfo->salted_password, client_key);
    _scram_h(client_key, SCRAM_KEY_LEN, stored_key);
    hmac_init(&ctx, stored_key, SCRAM_KEY_LEN);
    hmac_update(&ctx, (uint8_t*)cinfo->fmsg_bare->ptr, cinfo->fmsg_bare->len);
    hmac_update(&ctx, (const uint8_t*)",", 1);
    hmac_update(&ctx, (uint8_t*)cinfo->fmsg_srv->ptr, cinfo->fmsg_srv->len);
    hmac_update(&ctx, (const uint8_t*)",", 1);
    hmac_update(&ctx, (const uint8_t*)cinfo->fmsg_wproof->ptr, cinfo->fmsg_wproof->len);
    hmac_final(&ctx, clsign_key, SCRAM_KEY_LEN);
    for (int i = 0; i < SCRAM_KEY_LEN; ++i)
        result[i] = client_key[i] ^ clsign_key[i];
}

pgmsg_t *pgmsg_create_sasl_fin (pgmsg_resp_t *resp, conninfo_t *cinfo) {
    uint8_t cln_proof_key [SCRAM_KEY_LEN];
    _parse_scram_final(resp, cinfo);
    str_t *str = strfmt("c=biws,r=%s", cinfo->r_attr);
    cinfo->fmsg_wproof = mkcstr(str->ptr, str->len);
    strnadd(&str, CONST_STR_LEN(",p="));
    _calc_scram_proof(cinfo, cln_proof_key);
    cstr_t *cln_proof = cstr_b64encode((char*)cln_proof_key, SCRAM_KEY_LEN);
    strnadd(&str, cln_proof->ptr, cln_proof->len);
    pgmsg_t *msg = pgmsg_create(PG_PASS);
    _setstr(&msg, str->ptr, str->len);
    free(cln_proof);
    free(str);
    return msg;
}

int pgmsg_send (int fd, pgmsg_t *msg) {
    void *buf;
    size_t size;
    ssize_t sent = 0, wrote = 0;
    if ('\0' == msg->body.type) {
        buf = &msg->body.len;
        size = be32toh(msg->body.len);
    } else {
        buf = &msg->body;
        size = be32toh(msg->body.len) + sizeof(char);
    }
    while (sent < size) {
        do
            wrote = send(fd, buf, size - sent, 0);
        while (wrote < 0 && EINTR == errno);
        sent += wrote;
        buf += wrote;
    }
    return sent == size ? 0 : -1;
}

int pgmsg_recv (int fd, pgmsg_t **msg) {
    ssize_t readed = 0, total = 0;
    pgmsg_t *m;
    size_t bufsize;
    struct {
        char type;
        int32_t len;
    } __attribute__ ((packed)) header = { 0, 0 };
    while (total < HEADER_SIZE && (readed = recv(fd, (void*)(&header) + total, HEADER_SIZE - total, 0)) > 0)
        total += readed;
    if (-1 == readed)
        return -1;
    header.len = be32toh(header.len);
    bufsize = sizeof(pgmsg_t) + header.len;
    if (!(m = calloc(bufsize, sizeof(int8_t))))
        return -1;
    m->body.type = header.type;
    m->body.len = header.len;
    total = (header.len - sizeof(int32_t));
    char *ptr = m->body.ptr;
    while (total > 0) {
        if (-1 == (readed = recv(fd, ptr, total, 0))) {
            free(m);
            return -1;
        }
        total -= readed;
        ptr += readed;
    }
    *msg = m;
    return 0;
}

static int _parse_param_status (pgmsg_body_t *body, pgmsg_param_status_t *pmsg) {
    char *p = body->ptr, *e = p + body->len, *q = p;
    while (*q && q < e) ++q;
    if (q == e)
        return -1;
    pmsg->name = p;
    pmsg->value = q + 1;
    return 0;
}

static int _parse_error (pgmsg_body_t *body, pgmsg_error_t *pmsg) {
    char *p = body->ptr, *e = p + body->len;
    while (p < e) {
        switch (*p) {
            case PG_SEVERITY:
                pmsg->severity = ++p;
                break;
            case PG_FATAL:
                pmsg->text = ++p;
                break;
            case PG_SQLSTATE:
                pmsg->code = ++p;
                break;
            case PG_MESSAGE:
                pmsg->message = ++p;
                break;
            case PG_POSITION:
                pmsg->position = ++p;
                break;
            case PG_FILE:
                pmsg->file = ++p;
                break;
            case PG_LINE:
                pmsg->line = ++p;
                break;
            case PG_ROUTINE:
                pmsg->routine = ++p;
                break;
            case PG_DETAIL:
                pmsg->detail = ++p;
                break;
            case '\0':
                return 0;
            default:
                break;
        }
        while (p < e && *p) ++p;
        if (p == e)
            return 0;
        if (++p == e)
            return 0;
    }
    return 0;
}

static int _parse_rowdesc (pgmsg_t *msg, pgmsg_rowdesc_t *pmsg) {
    pmsg->nflds = _geti16(msg);
    for (int i = 0; i < pmsg->nflds; ++i) {
        const char *fname = _getstr(msg);
        if (!fname)
            return -1;
        pmsg->fields[i].fname = fname;
        pmsg->fields[i].oid_table = _geti32(msg);
        pmsg->fields[i].idx_field = _geti16(msg);
        pmsg->fields[i].oid_field = _geti32(msg);
        pmsg->fields[i].field_len = _geti16(msg);
        pmsg->fields[i].type_mod = _geti32(msg);
        pmsg->fields[i].field_fmt = _geti16(msg);
    }
    return 0;
}

static int _parse_datarow (pgmsg_t *msg, pgmsg_datarow_t *pmsg) {
    pmsg->nflds = _geti16(msg);
    for (int i = 0; i < pmsg->nflds; ++i) {
        int32_t len = pmsg->fields[i].len = _geti32(msg);
        pmsg->fields[i].data = NULL;
        if (len >= 0) {
            pmsg->fields[i].data = (uint8_t*)msg->pc;
            msg->pc += len;
        }
    }
    return 0;
}

static void _parse_copyin (pgmsg_t *msg, pgmsg_copyin_t *pmsg) {
    pmsg->fmt = _geti8(msg);
    pmsg->cols = _geti16(msg);
    pmsg->fmtcol = _geti16(msg);
}

pgmsg_resp_t *pgmsg_parse (pgmsg_t *msg) {
    pgmsg_resp_t *resp = NULL;
    msg->pc = msg->body.ptr;
    switch (msg->body.type) {
        case PG_AUTHOK:
            switch (be32toh(*((int32_t*)msg->body.ptr))) {
                case PG_OK:
                    resp = malloc(sizeof(pgmsg_auth_t));
                    resp->type = msg->body.type;
                    resp->msg_auth.success = be32toh(*((int32_t*)msg->body.ptr));
                    break;
                case PG_REQMD5:
                    resp = malloc(sizeof(pgmsg_auth_t));
                    resp->type = msg->body.type;
                    resp->msg_auth.success = be32toh(*((int32_t*)msg->body.ptr));
                    memcpy(resp->msg_auth.kind.md5_auth, msg->body.ptr + sizeof(int32_t), sizeof(uint8_t)*4);
                    break;
                case PG_REQSASL:
                    resp = malloc(sizeof(pgmsg_auth_t));
                    resp->type = msg->body.type;
                    resp->msg_auth.success = be32toh(*((int32_t*)msg->body.ptr));
                    break;
                case PG_SASLCON:
                case PG_SASLCOMP:
                    resp = malloc(sizeof(pgmsg_auth_t) + sizeof(sasl_t) + msg->body.len - sizeof(int32_t) * 2);
                    resp->type = msg->body.type;
                    resp->msg_auth.success = be32toh(*((int32_t*)msg->body.ptr));
                    resp->msg_auth.kind.sasl_auth.len = msg->body.len - sizeof(int32_t) * 2;
                    memcpy(resp->msg_auth.kind.sasl_auth.data, msg->body.ptr + sizeof(int32_t), resp->msg_auth.kind.sasl_auth.len);
                    break;
            }
            break;
        case PG_PARAMSTATUS:
            resp = malloc(sizeof(pgmsg_param_status_t));
            resp->type = msg->body.type;
            if (-1 == _parse_param_status(&msg->body, &resp->msg_param_status)) {
                free(resp);
                resp = NULL;
            }
            break;
        case PG_BACKENDKEYDATA:
            resp = malloc(sizeof(pgmsg_backend_keydata_t));
            resp->type = msg->body.type;
            resp->msg_backend_keydata.pid = be32toh(*((int32_t*)msg->body.ptr));
            resp->msg_backend_keydata.sk = be32toh(*((int32_t*)msg->body.ptr + sizeof(int32_t)));
            break;
        case PG_READY:
            resp = malloc(sizeof(pgmsg_ready_t));
            resp->type = msg->body.type;
            resp->msg_ready.tr = msg->body.ptr[0];
            break;
        case PG_TERM:
            resp =malloc(sizeof(pgmsg_auth_t));
            resp->type = msg->body.type;
            break;
        case PG_ERROR:
            resp = calloc(1, sizeof(pgmsg_error_t));
            resp->type = msg->body.type;
            if (-1 == _parse_error(&msg->body, &resp->msg_error)) {
                free(resp);
                resp = NULL;
            }
            break;
        case PG_ROWDESC:
            resp = malloc(sizeof(pgmsg_rowdesc_t) + sizeof(pgmsg_field_t) * be16toh(*((int16_t*)msg->body.ptr)));
            resp->type = msg->body.type;
            if (-1 == _parse_rowdesc(msg, &resp->msg_rowdesc)) {
                free(resp);
                resp = NULL;
            }
            break;
        case PG_DATAROW:
            resp = malloc(sizeof(pgmsg_datarow_t) + sizeof(pgmsg_data_t) * be16toh(*((int16_t*)msg->body.ptr)));
            resp->type = msg->body.type;
            if (-1 == _parse_datarow(msg, &resp->msg_datarow)) {
                free(resp);
                resp = NULL;
            }
            break;
        case PG_CMDCOMPLETE:
            resp = malloc(sizeof(pgmsg_cmd_complete_t));
            resp->type = msg->body.type;
            resp->msg_complete.tag = msg->body.ptr;
            break;
        case PG_COPYIN:
            resp = malloc(sizeof(pgmsg_copyin_t));
            resp->type = msg->body.type;
            _parse_copyin(msg, &resp->msg_copyin);
            break;
    }
    return resp;
}

static str_t *_prepare_str (const char *s, size_t l) {
    str_t *str = stralloc(l, 0);
    for (size_t i = 0; i < l; ++i) {
        switch (s[i]) {
            case '\\': strnadd(&str, CONST_STR_LEN("\\\\")); break;
            case '\b': strnadd(&str, CONST_STR_LEN("\\b")); break;
            case '\f': strnadd(&str, CONST_STR_LEN("\\f")); break;
            case '\n': strnadd(&str, CONST_STR_LEN("\\n")); break;
            case '\r': strnadd(&str, CONST_STR_LEN("\\r")); break;
            case '\t': strnadd(&str, CONST_STR_LEN("\\t")); break;
            default: strnadd(&str, &s[i], sizeof(char)); break;
        }
    }
    return str;
}

static int _copyin_field (pgmsg_t **msg, pgfld_t *fld) {
    struct tm tm;
    char buf [32];
    int len;
    if (fld->is_null)
        _setstr(msg, CONST_STR_LEN("\\N"));
    else if (PG_TEXT == fld->fmt) {
        str_t *str = _prepare_str(fld->data.s, fld->len);
        _setstr(msg, str->ptr, str->len);
        free(str);
    } else
    switch (fld->oid) {
        case OID_INT2:
            len = snprintf(buf, sizeof(buf),"%hd", be16toh(fld->data.i2));
            _setstr(msg, buf, len);
            break;
        case OID_INT4:
            len = snprintf(buf, sizeof(buf), "%d", be32toh(fld->data.i4));
            _setstr(msg, buf, len);
            break;
        case OID_INT8:
            len = snprintf(buf, sizeof(buf), LONG_FMT, be64toh(fld->data.i8));
            _setstr(msg, buf, len);
            break;
        case OID_FLOAT4:
            len = snprintf(buf, sizeof(buf), "%f", pg_conv_float(fld->data.f4));
            _setstr(msg, buf, len);
            break;
        case OID_FLOAT8:
            len = snprintf(buf, sizeof(buf), "%f", pg_conv_double(fld->data.f8));
            _setstr(msg, buf, len);
            break;
        case OID_VARCHAR:
        case OID_CHAR:
        case OID_TEXT: {
                str_t *str = _prepare_str(fld->data.s, fld->len);
                _setstr(msg, str->ptr, str->len);
                free(str);
            }
            break;
        case OID_BOOL:
            if (fld->data.b)
                _setstr(msg, CONST_STR_LEN("t"));
            else
                _setstr(msg, CONST_STR_LEN("f"));
            break;
        case OID_DATE:
            tm_dec(be64toh(fld->data.tm), &tm);
            len = snprintf(buf, sizeof(buf), "%d-%02d-%02d", tm.tm_year, tm.tm_mon, tm.tm_mday);
            _setstr(msg, buf, len);
            break;
        case OID_TIMESTAMP:
            tm_dec(be64toh(fld->data.tm), &tm);
            len = snprintf(buf, sizeof(buf), "%d-%02d-%02d %02d:%02d:%02d", tm.tm_year, tm.tm_mon, tm.tm_mday, tm.tm_hour, tm.tm_min, tm.tm_sec);
            _setstr(msg, buf, len);
            break;
        // OID_UUID
        case OID_BYTEA:
            _setstr(msg, CONST_STR_LEN("\\\\x"));
            if (!fld->len)
                _setstr(msg, CONST_STR_LEN("0"));
            else for (int i = 0; i < fld->len; ++i) {
                len = snprintf(buf, sizeof(buf), "%02x", fld->data.s[i]);
                _setstr(msg, buf, len);
            }
            break;
        case OID_BIT:
            len = snprintf(buf, sizeof(buf), "X'%08x'", fld->data.bit32.bit);
            _setstr(msg, buf, len);
            break;
        //OID_MONEY
        default:
            return -1;
    }
    return 0;
}

pgmsg_t *pgmsg_copyin_flds (int len, pgfld_t **flds) {
    pgmsg_t *msg = pgmsg_create(PG_COPYDATA);
    if (-1 == _copyin_field(&msg, flds[0]))
        goto err;
    for (int i = 1; i < len; ++i) {
        _setstr(&msg, CONST_STR_LEN("\t"));
        if (-1 == _copyin_field(&msg, flds[i]))
            goto err;
    }
    _seti8(&msg, 0x0a);
    return msg;
err:
    free(msg);
    return NULL;
}
