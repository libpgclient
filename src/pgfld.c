/*Copyright (c) Brian B.

  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 3 of the License, or (at your option) any later version.
  See the file LICENSE included with this distribution for more
  information.
*/
#include "libpgcli/pgfld.h"

//******************************************************************************
// Time functions
//******************************************************************************
#define MONTHS_PER_YEAR 12
#define POSTGRES_EPOCH_JDATE 2451545
#define INT64CONST(x)  (x##L)
#define SECS_PER_YEAR (36525 * 864)
#define SECS_PER_DAY 86400
#define SECS_PER_HOUR 3600
#define SECS_PER_MINUTE 60
#define MINS_PER_HOUR 60
#define USECS_PER_DAY INT64CONST(86400000000)
#define USECS_PER_HOUR INT64CONST(3600000000)
#define USECS_PER_MINUTE INT64CONST(60000000)
#define USECS_PER_SEC INT64CONST(1000000)
#define MIN_TIMESTAMP INT64CONST(-211813488000000000)
#define END_TIMESTAMP INT64CONST(9223371331200000000)
#define IS_VALID_TIMESTAMP(t)  (MIN_TIMESTAMP <= (t) && (t) < END_TIMESTAMP)
#define TMODULO(t,q,u) \
do { \
    (q) = ((t) / (u)); \
    if ((q) != 0) (t) -= ((q) * (u)); \
} while(0)
#define JULIAN_MINYEAR (-4713)
#define JULIAN_MINMONTH (11)
#define JULIAN_MINDAY (24)
#define JULIAN_MAXYEAR (5874898)
#define JULIAN_MAXMONTH (6)
#define JULIAN_MAXDAY (3)
#define IS_VALID_JULIAN(y,m,d) \
    (((y) > JULIAN_MINYEAR || \
      ((y) == JULIAN_MINYEAR && ((m) >= JULIAN_MINMONTH))) && \
     ((y) < JULIAN_MAXYEAR || \
      ((y) == JULIAN_MAXYEAR && ((m) < JULIAN_MAXMONTH))))

void j2date(int jd, int *year, int *month, int *day) {
    unsigned int julian;
    unsigned int quad;
    unsigned int extra;
    int y;
    julian = jd;
    julian += 32044;
    quad = julian / 146097;
    extra = (julian - quad * 146097) * 4 + 3;
    julian += 60 + quad * 3 + extra / 146097;
    quad = julian / 1461;
    julian -= quad * 1461;
    y = julian * 4 / 1461;
    julian = ((y != 0) ? ((julian + 305) % 365) : ((julian + 306) % 366)) + 123;
    y += quad * 4;
    *year = y - 4800;
    quad = julian * 2141 / 65536;
    *day = julian - 7834 * quad / 256;
    *month = (quad + 10) % MONTHS_PER_YEAR + 1;
}

int date2j(int y, int m, int d) {
    int julian;
    int century;
    if (m > 2) {
        m += 1;
        y += 4800;
    } else {
        m += 13;
        y += 4799;
    }
    century = y / 100;
    julian = y * 365 - 32167;
    julian += y / 4 - century + century / 4;
    julian += 7834 * m / 256 + d;

    return julian;
}

static void dt2time(int64_t jd, int *hour, int *min, int *sec) {
    tm_t time;
    time = jd;
    *hour = time / USECS_PER_HOUR;
    time -= (*hour) * USECS_PER_HOUR;
    *min = time / USECS_PER_MINUTE;
    time -= (*min) * USECS_PER_MINUTE;
    *sec = time / USECS_PER_SEC;
}

int timestamp_to_tm (tm_t dt, struct tm *tm) {
    tm_t date, time = dt;
    memset(tm, 0, sizeof(struct tm));
    TMODULO(time, date, USECS_PER_DAY);
    if (time < INT64CONST(0)) {
        time += USECS_PER_DAY;
        date -= 1;
    }
    date += POSTGRES_EPOCH_JDATE;
    if (date < 0 || date > (int64_t)INT_MAX)
        return -1;
    j2date((int)date, &tm->tm_year, &tm->tm_mon, &tm->tm_mday);
    dt2time(time, &tm->tm_hour, &tm->tm_min, &tm->tm_sec);
    return 0;
}

static int64_t time2t(const int hour, const int min, const int sec, const fsec_t fsec) {
    return (((((hour * MINS_PER_HOUR) + min) * SECS_PER_MINUTE) + sec) * USECS_PER_SEC) + fsec;
}

static tm_t dt2local(tm_t dt, int tz) {
    dt -= (tz * USECS_PER_SEC);
    return dt;
}

int tm_to_timestamp (struct tm *tm, fsec_t fsec, int *tzp, tm_t *result) {
    int dDate;
    int64_t time;
    if (!IS_VALID_JULIAN(tm->tm_year, tm->tm_mon, tm->tm_mday))
        return -1;
    dDate = date2j(tm->tm_year, tm->tm_mon, tm->tm_mday) - date2j(2000, 1, 1);
    time = time2t(tm->tm_hour, tm->tm_min, tm->tm_sec, fsec);
    *result = (dDate * USECS_PER_DAY) + time;
    if ((*result - time) / USECS_PER_DAY != dDate)
        return -1;
    if ((*result < 0 && dDate > 0) ||
        (*result > 0 && dDate < -1))
        return -1;
    if (tzp != NULL)
        *result = dt2local(*result, -(*tzp));
    if (!IS_VALID_TIMESTAMP(*result))
        return -1;
    return 0;
}


struct tm *date_to_tm (date_t dt, struct tm *tm) {
    memset(tm, 0, sizeof(struct tm));
    j2date(dt + POSTGRES_EPOCH_JDATE, &tm->tm_year, &tm->tm_mon, &tm->tm_mday);
    return tm;
}

date_t tm_to_date (struct tm *tm) {
    date_t dt = date2j(tm->tm_year, tm->tm_mon, tm->tm_mday);
    return dt - POSTGRES_EPOCH_JDATE;
}

struct tm *interval_to_tm(pg_intv_t *span, struct tm *tm) {
    tm_t time;
    if (span->month != 0) {
        tm->tm_year = span->month / MONTHS_PER_YEAR;
        tm->tm_mon = span->month % MONTHS_PER_YEAR;
    } else {
        tm->tm_year = 0;
        tm->tm_mon = 0;
    }
    time = span->time;
    tm->tm_mday = time / USECS_PER_DAY;
    time -= tm->tm_mday * USECS_PER_DAY;
    tm->tm_hour = time / USECS_PER_HOUR;
    time -= tm->tm_hour * USECS_PER_HOUR;
    tm->tm_min = time / USECS_PER_MINUTE;
    time -= tm->tm_min * USECS_PER_MINUTE;
    tm->tm_sec = time / USECS_PER_SEC;
    return tm;
}

//******************************************************************************
// Double function
//******************************************************************************
double pg_conv_double (double d) {
    union {
        double d;
        unsigned char bytes [8];
    } src, dst;
    src.d = d;
    dst.bytes[0] = src.bytes[7];
    dst.bytes[1] = src.bytes[6];
    dst.bytes[2] = src.bytes[5];
    dst.bytes[3] = src.bytes[4];
    dst.bytes[4] = src.bytes[3];
    dst.bytes[5] = src.bytes[2];
    dst.bytes[6] = src.bytes[1];
    dst.bytes[7] = src.bytes[0];
    return dst.d;
}

float pg_conv_float (float f) {
    union {
        float f;
        unsigned char bytes [4];
    } src, dst;
    src.f = f;
    dst.bytes[0] = src.bytes[3];
    dst.bytes[1] = src.bytes[2];
    dst.bytes[2] = src.bytes[1];
    dst.bytes[3] = src.bytes[0];
    return dst.f;
}

//******************************************************************************
// Numeric functions
//******************************************************************************
#ifdef HAVE_GMP
#define MAX_DIGITS (sizeof(mp_limb_t) / sizeof(int16_t))
static void set_digits_n (mpz_t x, int16_t *p_digits, int16_t *digits, int len, int weight) {
    int i = 1;
    mpz_t m;
    mpz_init_set_ui(m, 1);
    if (p_digits < digits)
        mpz_set_ui(x, 0);
    else
        mpz_set_ui(x, *p_digits);
    p_digits++;
    if (len < weight) {
        while (i < len) {
            if (p_digits < digits)
                mpz_mul_si(m, m, 10000);
            else
                mpz_set_ui(m, 10000);
            mpz_mul(x, x, m);
            mpz_add_ui(x, x, *p_digits++);
            i++;
        }
        while (i < weight) {
            mpz_mul_si(x, x, 10000);
            ++i;
        }
    } else {
        while (i < weight) {
            if (p_digits < digits)
                mpz_mul_si(m, m, 10000);
            else
                mpz_set_ui(m, 10000);
            mpz_mul(x, x, m);
            mpz_add_ui(x, x, *p_digits++);
            i++;
        }
    }
    mpz_clear(m);
}

static void set_digits_d (mpz_t x, int count) {
    int i = 0;
    while (i < count) {
        mpz_mul_si(x, x, 10000);
        i++;
    }
}

void pg_get_numeric (uint8_t *buf, mpq_t res, uint16_t *scale) {
    int16_t len = be16toh(*(int16_t*)buf); buf += sizeof(int16_t);
    int16_t weight = be16toh(*(int16_t*)buf); buf += sizeof(int16_t);
    int16_t sign = be16toh(*(int16_t*)buf); buf += sizeof(int16_t);
    mpq_init(res);
    if (sign == NUMERIC_POS || sign == NUMERIC_NEG || sign == NUMERIC_NAN) {
        uint16_t dscale = be16toh(*(uint16_t*)buf); buf += sizeof(uint16_t);
        if (scale)
            *scale = dscale;
        if ((dscale & NUMERIC_DSCALE_MASK) == dscale) {
            if (0 == len)
                return;
            mpq_t x, y;
            mpq_ptr q = x;
            mpz_ptr n = &q->_mp_num, d = &q->_mp_den;
            int16_t *digits = malloc(len * sizeof(int16_t)), *p_digits = digits;
            for (int i = 0; i < len; ++i)
                digits[i] = be16toh(*(int16_t*)buf);
            buf += sizeof(int16_t);
            ++weight;
            mpz_init(n);
            if (weight > 0)
                set_digits_n(n, p_digits, digits, len, weight);
            mpz_init(d);
            mpz_set_si(d, 1);
            int cnt = len - weight;
            if (cnt <= 0)
                mpq_set(res, q);
            else {
                p_digits += weight;
                q = y;
                n = &q->_mp_num;
                d = &q->_mp_den;
                mpz_init(n);
                mpz_init_set_si(d, 1);
                set_digits_n(n, p_digits, digits, cnt, cnt);
                set_digits_d(d, cnt);
                mpq_add(res, x, y);
                mpq_clear(y);
            }
            if (sign == NUMERIC_NEG)
                mpq_neg(res, res);
            free(digits);
            mpq_clear(x);
        }
    }
    return;
}

str_t *pg_numstr (mpq_ptr x, int base, uint16_t dscale, int float_prec) {
    char *s;
    mpf_t f;
    mp_exp_t exp;
    if (0 == dscale) {
        mpz_ptr z = &x->_mp_num;
        if (!(s = mpz_get_str(NULL, base, z)))
            return NULL;
        str_t *res = mkstr(s, strlen(s), 8);
        free(s);
        return res;
    }
    mpf_init2(f, float_prec);
    mpf_set_q(f, x);
    if ((s = mpf_get_str(NULL, &exp, base, 0, f))) {
        str_t *res;
        ssize_t n;
        char *e, *p;
        if (exp > 0) {
            res = mkstr(s, strlen(s), 8);
            if (exp > res->len) {
                strpad(&res, exp, '0', STR_LEFT);
                strnadd(&res, CONST_STR_LEN(".0"));
            } else
                strepl(&res, res->ptr + exp, 0, CONST_STR_LEN("."));
        } else {
            res = mkstr(CONST_STR_LEN("0."), 8);
            if (exp < 0)
                strpad(&res, -(exp-2), '0', STR_LEFT);
            strnadd(&res, s, strlen(s));
            exp = 1;
        }
        p = res->ptr + exp;
        free(s);
        e = res->ptr + res->len;
        if (0 < (n = dscale - ((intptr_t)e - (intptr_t)p - 1)))
            strpad(&res, res->len + n, '0', STR_LEFT);
        mpf_clear(f);
        return res;
    }
    mpf_clear(f);
    return NULL;
}
#endif
