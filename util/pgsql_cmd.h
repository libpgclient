/*Copyright (c) Brian B.

  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 3 of the License, or (at your option) any later version.
  See the file LICENSE included with this distribution for more
  information.
*/
#ifndef __PGSQL_CMD_H__
#define __PGSQL_CMD_H__

#include <stdio.h>
#include <libex/str.h>
#include "libpgcli/pgconn.h"

void print_result (pgconn_t *conn);
cstr_t *get_dbsize (pgconn_t *conn, char *dbname);
cstr_t *get_current_schema (pgconn_t *conn);
cstr_t *get_tablesize (pgconn_t *conn, char *schema, char *table);
cstr_t *get_tablerecs (pgconn_t *conn, char *schema, char *table);

void show (pgconn_t *conn, char **line, size_t *line_len);
void show_table (pgconn_t *conn, char **line, size_t *line_len);
void show_tables (pgconn_t *conn);

#endif
