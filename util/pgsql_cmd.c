/*Copyright (c) Brian B.

  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 3 of the License, or (at your option) any later version.
  See the file LICENSE included with this distribution for more
  information.
*/
#include "pgsql_cmd.h"

void show (pgconn_t *conn, char **line, size_t *line_len) {
    strptr_t tok = { .ptr = NULL, .len = 0 };
    strntok(line, line_len, CONST_STR_LEN(STR_SPACES), &tok);
    if (-1 == strntok(line, line_len, CONST_STR_LEN(STR_SPACES), &tok))
        return;
    if (0 == cmpstr(tok.ptr, tok.len, CONST_STR_LEN("table")))
        show_table(conn, line, line_len);
    else if (0 == cmpstr(tok.ptr, tok.len, CONST_STR_LEN("tables")))
        show_tables(conn);
}

cstr_t *get_current_schema (pgconn_t *conn) {
    pg_execsql(conn, CONST_STR_LEN("select current_schema()"));
    return pg_getstr(conn, 0, 0);
}

cstr_t *get_dbsize (pgconn_t *conn, char *dbname) {
    str_t *sql = strfmt("SELECT pg_size_pretty(pg_database_size('%s'))", dbname);
    pg_execsql(conn, sql->ptr, sql->len);
    cstr_t *res = pg_getstr(conn, 0, 0);
    free(sql);
    return res;
}

cstr_t *get_tablesize (pgconn_t *conn, char *schema, char *table) {
    str_t *sql = strfmt("SELECT pg_size_pretty(pg_total_relation_size('%s.%s'))",
        schema, table);
    pg_execsql(conn, sql->ptr, sql->len);
    cstr_t *res = pg_getstr(conn, 0, 0);
    free(sql);
    return res;
}

cstr_t *get_tablerecs (pgconn_t *conn, char *schema, char *table) {
    str_t *sql = strfmt("SELECT count(*) from %s.%s",
        schema, table);
    pg_execsql(conn, sql->ptr, sql->len);
    cstr_t *res = pg_getstr(conn, 0, 0);
    free(sql);
    return res;
}

static int *get_max_lens (pgconn_t *conn) {
    int *max_lens = calloc(pg_nrecs(conn), sizeof(int));
    
    for (int i = 0; i < pg_nrecs(conn); ++i)
        for (int j = 0; j < pg_nflds(conn); ++j)
            if (!pg_isnull(conn, i, j) && pg_fldlen(conn, i, j) > max_lens[j])
                max_lens[j] = pg_fldlen(conn, i, j);

    for (int i = 0; i < pg_nflds(conn); ++i) {
        int l = strlen(conn->rowdesc->fields[i].fname);
        if (l > max_lens[i]) max_lens[i] = l;
    }

    return max_lens;
}

static void print_field_names (pgconn_t *conn, int *max_lens) {
    int max_len = 0;
    for (int i = 0; i < pg_nflds(conn); ++i) {
        const char *fldnam = conn->rowdesc->fields[i].fname;
        cstr_t *s = mkcstrpad(fldnam, strlen(fldnam), ' ', max_lens[i]);
        max_len += max_lens[i];
        for (int j = 0; j < s->len; ++j)
            s->ptr[j] = toupper(s->ptr[j]);
        if (0 == i)
            printf("|%s|", s->ptr);
        else
            printf("%s|", s->ptr);
        free(s);
    }
    cstr_t *s = mkcstrfill(max_len+pg_nflds(conn), '-');
    printf("\n%s\n", s->ptr);
    free(s);
}

void print_result (pgconn_t *conn) {
    int *max_lens = get_max_lens(conn);

    print_field_names(conn, max_lens);
    for (int i = 0; i < pg_nrecs(conn); ++i) {
        for (int j = 0; j < pg_nflds(conn); ++j) {
            cstr_t *str;
            if (pg_isnull(conn, i, j))
                str = mkcstrfill(max_lens[j], ' ');
            else {
                strptr_t s = pg_get(conn, i, j);
                str = mkcstrpad(s.ptr, s.len, ' ', max_lens[j]);
            }
            if (0 == j)
                printf("|%s|", str->ptr);
            else
                printf("%s|", str->ptr);
            free(str);
        }
        printf("\n");
    }

    free(max_lens);
}

typedef struct {
    cstr_t *name;
    cstr_t *size;
} table_t;
DECLARE_LIST(tables,table_t)
DECLARE_LIST_FUN(tables,table_t)

typedef struct {
    int *max_lens;
    pgconn_t *conn;
} stmt_info_t;

static void clear_table (table_t *t) {
    if (t->name) free(t->name);
    if (t->size) free(t->size);
}

static void print_table_field (cstr_t *val, int max_len) {
    cstr_t *s;
    if (val)
        s = mkcstrpad(val->ptr, val->len, ' ', max_len);
    else
        s = mkcstrfill(max_len, ' ');
    printf("|%s", s->ptr);
    free(s);
}

static void print_table_size (pgconn_t *conn, cstr_t *table_name) {
    str_t *sql = strfmt("select count(*) from %s", table_name->ptr);
    cstr_t *s = NULL;
    if (0 == pg_execsql(conn, sql->ptr, sql->len))
        s = pg_getstr(conn, 0, 0);
    if (s) printf("|%s\n", s->ptr); else printf("|0\n");
    free(s);
    free(sql);
}

static int print_table (table_t *t, void *userdata) {
    stmt_info_t *si = userdata;

    print_table_field(t->name, si->max_lens[0]);
    print_table_field(t->size, si->max_lens[1]);
    print_table_size(si->conn, t->name);

    return ENUM_CONTINUE;
}

static size_t print_header_col (const char *name, size_t name_len, int len) {
    size_t rc;
    cstr_t *s = mkcstrpad(name, name_len, ' ', len);
    printf("|%s", s->ptr);
    rc = s->len;
    free(s);
    return rc;
}

static void print_tables_header (int *max_lens) {
    size_t n = print_header_col(CONST_STR_LEN("NAME"), max_lens[0]);
    n += print_header_col(CONST_STR_LEN("SIZE"), max_lens[1]);
    n += print_header_col(CONST_STR_LEN("RECORDS"), max_lens[2]);
    cstr_t *s = mkcstrfill(n + 3, '-');
    printf("\n%s\n", s->ptr);
    free(s);
}

#define SQL_TABLES \
    "select distinct i.table_name," \
    "  pg_size_pretty(pg_total_relation_size(i.table_schema || '.' || i.table_name)) " \
    "from information_schema.tables i, pg_catalog.pg_tables p " \
    "where i.table_schema = p.schemaname" \
    "  and tableowner <> 'postgres'" \
    "  and table_type = 'BASE TABLE'" \
    "  and i.table_schema = current_schema()"
void show_tables (pgconn_t *conn) {
    pg_execsql(conn, CONST_STR_LEN(SQL_TABLES));

    if (!pg_nrecs(conn)) {
        printf("Nothing\n");
        return;
    }

    stmt_info_t si = {
        .max_lens = get_max_lens(conn),
        .conn = conn,
    };
    tables_t *tables = create_tables();

    tables->on_free = clear_table;
    for (int i = 0; i < pg_nrecs(conn); ++i) {
        table_t item = {
            .name = pg_getstr(conn, i, 0),
            .size = pg_getstr(conn, i, 1)
        };
        add_tail_tables(tables, &item, 0);
    }
    pg_close(conn);

    print_tables_header(si.max_lens);
    foreach_tables(tables, print_table, &si, 0);

    free_tables(tables);
    free(si.max_lens);
}

#define SQL_TABLE \
    "select ordinal_position, column_name, case is_nullable when 'YES' then '' else 'not null' end case, data_type, character_maximum_length " \
    "from information_schema.columns " \
    "where table_schema = '%s' and table_name = '%s'"
void show_table (pgconn_t *conn, char **line, size_t *line_len) {
    str_t *sql;
    strptr_t tok = { .ptr = NULL, .len = 0 };
    cstr_t *schema, *table;
    char *p, *e;

    if (-1 == strntok(line, line_len, CONST_STR_LEN(STR_SPACES), &tok))
        return;
    e = tok.ptr + tok.len;
    if ((p = strnchr(tok.ptr, '.', tok.len))) {
        schema = mkcstr(tok.ptr, (uintptr_t)p - (uintptr_t)tok.ptr);
        table = mkcstr(p+1, (uintptr_t)e - (uintptr_t)p-1);
    } else {
        schema = get_current_schema(conn);
        table = mkcstr(tok.ptr, tok.len);
    }

    sql = strfmt(SQL_TABLE, schema->ptr, table->ptr);
    if (!pg_execsql(conn, sql->ptr, sql->len)) {
        if (pg_nrecs(conn))
            print_result(conn);
        else
            printf("Nothing\n");
    }
    free(sql);
    pg_close(conn);

    cstr_t *table_size = get_tablesize(conn, schema->ptr, table->ptr),
           *table_nrecs = get_tablerecs(conn, schema->ptr, table->ptr);
    printf("Table size: %s; Records: %s\n", table_size->ptr, table_nrecs->ptr);
    free(table_nrecs);
    free(table_size);

    free(schema);
    free(table);
}
