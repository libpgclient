/*Copyright (c) Brian B.

  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 3 of the License, or (at your option) any later version.
  See the file LICENSE included with this distribution for more
  information.

  Portions Copyright (c) 1994, Regents of the University of California

*/
#include <string.h>
#include "hmac.h"

static const uint32_t sha_init_value [8] = {
    0x6a09e667UL,
    0xbb67ae85UL,
    0x3c6ef372UL,
    0xa54ff53aUL,
    0x510e527fUL,
    0x9b05688cUL,
    0x1f83d9abUL,
    0x5be0cd19UL
};

static const uint32_t K256[64] = {
    0x428a2f98UL, 0x71374491UL, 0xb5c0fbcfUL, 0xe9b5dba5UL,
    0x3956c25bUL, 0x59f111f1UL, 0x923f82a4UL, 0xab1c5ed5UL,
    0xd807aa98UL, 0x12835b01UL, 0x243185beUL, 0x550c7dc3UL,
    0x72be5d74UL, 0x80deb1feUL, 0x9bdc06a7UL, 0xc19bf174UL,
    0xe49b69c1UL, 0xefbe4786UL, 0x0fc19dc6UL, 0x240ca1ccUL,
    0x2de92c6fUL, 0x4a7484aaUL, 0x5cb0a9dcUL, 0x76f988daUL,
    0x983e5152UL, 0xa831c66dUL, 0xb00327c8UL, 0xbf597fc7UL,
    0xc6e00bf3UL, 0xd5a79147UL, 0x06ca6351UL, 0x14292967UL,
    0x27b70a85UL, 0x2e1b2138UL, 0x4d2c6dfcUL, 0x53380d13UL,
    0x650a7354UL, 0x766a0abbUL, 0x81c2c92eUL, 0x92722c85UL,
    0xa2bfe8a1UL, 0xa81a664bUL, 0xc24b8b70UL, 0xc76c51a3UL,
    0xd192e819UL, 0xd6990624UL, 0xf40e3585UL, 0x106aa070UL,
    0x19a4c116UL, 0x1e376c08UL, 0x2748774cUL, 0x34b0bcb5UL,
    0x391c0cb3UL, 0x4ed8aa4aUL, 0x5b9cca4fUL, 0x682e6ff3UL,
    0x748f82eeUL, 0x78a5636fUL, 0x84c87814UL, 0x8cc70208UL,
    0x90befffaUL, 0xa4506cebUL, 0xbef9a3f7UL, 0xc67178f2UL
};

void sha_init (sha_t *ctx) {
    memcpy(ctx->state, sha_init_value, SHA_DIGEST_LEN);
    memset(ctx->buf, 0, SHA_BLOCK_LEN);
    ctx->bitcount = 0;
}

#define SHA_SHORT_BLOCK_LEN (SHA_BLOCK_LEN - 8)

#define R(b,x) ((x) >> (b))
#define S32(b,x) (((x) >> (b)) | ((x) << (32 - (b))))
#define Ch(x,y,z) (((x) & (y)) ^ ((~(x)) & (z)))
#define Maj(x,y,z) (((x) & (y)) ^ ((x) & (z)) ^ ((y) & (z)))
#define Sigma0(x) (S32(2,  (x)) ^ S32(13, (x)) ^ S32(22, (x)))
#define Sigma1(x) (S32(6,  (x)) ^ S32(11, (x)) ^ S32(25, (x)))
#define sigma0(x) (S32(7,  (x)) ^ S32(18, (x)) ^ R(3 ,   (x)))
#define sigma1(x) (S32(17, (x)) ^ S32(19, (x)) ^ R(10,   (x)))

#if BYTE_ORDER == LITTLE_ENDIAN
#define REVERSE32(w,x) { \
    uint32_t tmp = (w); \
    tmp = (tmp >> 16) | (tmp << 16); \
    (x) = ((tmp & 0xff00ff00UL) >> 8) | ((tmp & 0x00ff00ffUL) << 8); \
}
#define REVERSE64(w,x) { \
    uint64_t tmp = (w); \
    tmp = (tmp >> 32) | (tmp << 32); \
    tmp = ((tmp & 0xff00ff00ff00ff00ULL) >> 8) | \
        ((tmp & 0x00ff00ff00ff00ffULL) << 8); \
    (x) = ((tmp & 0xffff0000ffff0000ULL) >> 16) | \
        ((tmp & 0x0000ffff0000ffffULL) << 16); \
}
#endif

static void sha_transform (sha_t *ctx, const uint8_t *data) {
    uint32_t a = ctx->state[0],
             b = ctx->state[1],
             c = ctx->state[2],
             d = ctx->state[3],
             e = ctx->state[4],
             f = ctx->state[5],
             g = ctx->state[6],
             h = ctx->state[7],
             s0, s1, T1, T2, *w256 = (uint32_t*)ctx->buf;
    int j = 0;
    do {
        w256[j] = (uint32_t)data[3] | ((uint32_t)data[2] << 8) | ((uint32_t)data[1] << 16) | ((uint32_t)data[0] << 24);
        data += 4;
        T1 = h + Sigma1(e) + Ch(e, f, g) + K256[j] + w256[j];
        T2 = Sigma0(a) + Maj(a, b, c);
        h = g;
        g = f;
        f = e;
        e = d + T1;
        d = c;
        c = b;
        b = a;
        a = T1 + T2;
        j++;
    } while (j < 16);
    do {
        s0 = w256[(j+1) & 0x0f];
        s0 = sigma0(s0);
        s1 = w256[(j+14) & 0x0f];
        s1 = sigma1(s1);
        T1 = h + Sigma1(e) + Ch(e, f, g) + K256[j] + (w256[j & 0x0f] += s1 + w256[(j + 9) & 0x0f] + s0);
        T2 = Sigma0(a) + Maj(a, b, c);
        h = g;
        g = f;
        f = e;
        e = d + T1;
        d = c;
        c = b;
        b = a;
        a = T1 + T2;
        j++;
    } while (j < 64);
    ctx->state[0] += a;
    ctx->state[1] += b;
    ctx->state[2] += c;
    ctx->state[3] += d;
    ctx->state[4] += e;
    ctx->state[5] += f;
    ctx->state[6] += g;
    ctx->state[7] += h;
}

void sha_update (sha_t *ctx, const uint8_t *data, size_t len) {
    size_t f, u;
    if (0 == len) return;
    if ((u = (ctx->bitcount >> 3) % SHA_BLOCK_LEN) > 0) {
        f = SHA_BLOCK_LEN - u;
        if (len >= f) {
            memcpy(&ctx->buf[u], data, f);
            ctx->bitcount += f << 3;
            len -= f;
            data += f;
            sha_transform(ctx, ctx->buf);
        } else {
            memcpy(&ctx->buf[u], data, len);
            ctx->bitcount += len << 3;
            return;
        }
    }
    while (len >= SHA_BLOCK_LEN) {
        sha_transform(ctx, data);
        ctx->bitcount += SHA_BLOCK_LEN << 3;
        len -= SHA_BLOCK_LEN;
        data += SHA_BLOCK_LEN;
    }
    if (len > 0) {
        memcpy(ctx->buf, data, len);
        ctx->bitcount += len << 3;
    }
}

static void sha_last (sha_t *ctx) {
    uint32_t u = (ctx->bitcount >> 3) % SHA_BLOCK_LEN;
    #if BYTE_ORDER == LITTLE_ENDIAN
    REVERSE64(ctx->bitcount, ctx->bitcount);
    #endif
    if (u > 0) {
        ctx->buf[u++] = 0x80;
        if (u <= SHA_SHORT_BLOCK_LEN)
            memset(&ctx->buf[u], 0, SHA_SHORT_BLOCK_LEN - u);
        else {
            if (u < SHA_BLOCK_LEN)
                memset(&ctx->buf[u], 0, SHA_BLOCK_LEN - u);
            sha_transform(ctx, ctx->buf);
            memset(ctx->buf, 0, SHA_SHORT_BLOCK_LEN);
        }
    } else {
        memset(ctx->buf, 0, SHA_SHORT_BLOCK_LEN);
        *ctx->buf = 0x80;
    }
    *(uint64_t*)&ctx->buf[SHA_SHORT_BLOCK_LEN] = ctx->bitcount;
    sha_transform(ctx, ctx->buf);
}

void sha_final (sha_t *ctx, uint8_t *digest) {
    if (digest) {
        sha_last(ctx);
        #if BYTE_ORDER == LITTLE_ENDIAN
        for (int j = 0; j < 8; ++j)
            REVERSE32(ctx->state[j], ctx->state[j]);
        #endif
        memcpy(digest, ctx->state, SHA_DIGEST_LEN);
    }
    memset(ctx, 0, sizeof(sha_t));
}

void hmac_init (hmac_t *ctx, uint8_t *key, size_t len) {
    uint8_t sbuf [SHA_DIGEST_LEN];
    memset(ctx->opad, HMAC_OPAD, SHA_BLOCK_LEN);
    memset(ctx->ipad, HMAC_IPAD, SHA_BLOCK_LEN);
    if (len > SHA_BLOCK_LEN) {
        memset(sbuf, 0, sizeof sbuf);
        sha_init(&ctx->hash);
        sha_update(&ctx->hash, key, len);
        sha_final(&ctx->hash, sbuf);
        key = sbuf;
        len = SHA_DIGEST_LEN;
    }
    for (int i = 0; i < len; ++i) {
        ctx->ipad[i] ^= key[i];
        ctx->opad[i] ^= key[i];
    }
    sha_init(&ctx->hash);
    sha_update(&ctx->hash, ctx->ipad, SHA_BLOCK_LEN);
}

void hmac_update (hmac_t *ctx, const uint8_t *data, size_t len) {
    sha_update(&ctx->hash, data, len);
}

void hmac_final (hmac_t *ctx, uint8_t *dst, size_t len) {
    uint8_t *x = calloc(SHA_DIGEST_LEN, sizeof(uint8_t));
    sha_final(&ctx->hash, x);
    sha_init(&ctx->hash);
    sha_update(&ctx->hash, ctx->opad, SHA_BLOCK_LEN);
    sha_update(&ctx->hash, x, SHA_DIGEST_LEN);
    sha_final(&ctx->hash, dst);
    free(x);
}
