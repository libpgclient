/*Copyright (c) Brian B.

  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 3 of the License, or (at your option) any later version.
  See the file LICENSE included with this distribution for more
  information.
*/
#ifndef __PGPROV3_H__
#define __PGPROV3_H__

#include <stdint.h>
#include <errno.h>
#include <stdarg.h>
#include <limits.h>
#include <termios.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <libex/str.h>
#include <libex/array.h>
#include <libex/endian.h>
#include "pgfld.h"

#define PG_MAJOR_VER 3
#define PG_MINOR_VER 0

#define PG_AUTHOK 'R'
#define PG_PASS 'p'
#define PG_PARAMSTATUS 'S'
#define PG_BACKENDKEYDATA 'K'
#define PG_READY 'Z'
#define PG_TERM 'X'
#define PG_ERROR 'E'
#define PG_SIMPLEQUERY 'Q'
#define PG_ROWDESC 'T'
#define PG_DATAROW 'D'
#define PG_CMDCOMPLETE 'C'
#define PG_PARSE 'P'
#define PG_PARSECOMPLETE '1'
#define PG_BIND 'B'
#define PG_BINDCOMPLETE '2'
#define PG_DESCRIBE 'D'
#define PG_EXECUTE 'E'
#define PG_SYNC 'S'
#define PG_NODATA 'n'
#define PG_PARAMDESC 't'
#define PG_PORTALSUSPENDED 's'
#define PG_EMPTYQUERY 'I'
#define PG_CLOSE 'C'
#define PG_COPYIN 'G'
#define PG_COPYDATA 'd'
#define PG_COPYEND 'c'

#define PG_IDLE 'I'
#define PG_ACTIVE 'T'
#define PG_EXCEPT 'E'

#define PG_SEVERITY 'S'
#define PG_FATAL 'V'
#define PG_SQLSTATE 'C'
#define PG_MESSAGE 'M'
#define PG_POSITION 'P'
#define PG_FILE 'F'
#define PG_LINE 'L'
#define PG_ROUTINE 'R'
#define PG_DETAIL 'D'

#define PG_OPNAME 'S'
#define PG_PORTAL 'P'

#define PG_PARAM_END -1

#define PG_TEXT 0
#define PG_BIN 1

#define PG_PREPARED_OPERATOR 'S'
#define PG_PREPARED_PORTAL 'P'

#define PG_FULL 0

#define PG_OK 0
#define PG_REQMD5 5
#define PG_REQPASS 3
#define PG_REQSASL 10
#define PG_SASLCON 11
#define PG_SASLCOMP 12

#define PG_MD5PASS_LEN 35

#define SCRAM_RAW_NONCE_LEN 18
#define SCRAM_NONCE_LEN 24
#define SCRAM_KEY_LEN 32

typedef int32_t oid_t;

typedef struct {
    char type;
    int32_t len;
    char ptr [0];
} __attribute__ ((packed)) pgmsg_body_t;

typedef struct pgmsg pgmsg_t;
struct pgmsg {
    pgmsg_t *prev;
    pgmsg_t *next;
    size_t bufsize;
    size_t len;
    char *pc;
    pgmsg_body_t body;
};

typedef struct {
    size_t len;
    pgmsg_t *head;
} pgmsg_list_t;
void pgmsg_list_add (pgmsg_list_t *list, pgmsg_t *msg);
void pgmsg_list_clear (pgmsg_list_t *list);

typedef struct {
    struct sockaddr_in in_addr;
    char *user;
    char *pass;
    char salt [4];
    char srv_sign [SCRAM_KEY_LEN+1];
    str_t *srv_scram_msg;
    char *r_attr;
    char *s_attr;
    char *i_attr;
    char salted_password [SCRAM_KEY_LEN];
    cstr_t *nonce;
    cstr_t *fmsg_bare;
    cstr_t *fmsg_srv;
    cstr_t *fmsg_wproof;
} conninfo_t;

typedef struct {
    int32_t len;
    uint8_t data [0];
} sasl_t;

typedef struct {
    char type;
    int32_t success;
    union {
        uint8_t md5_auth [4];
        sasl_t sasl_auth;
    } kind;
} pgmsg_auth_t;

typedef struct {
    int32_t kind;
} pgmsg_sasl_initresp_t;

typedef struct {
    char type;
    const char *name;
    const char *value;
} pgmsg_param_status_t;

typedef struct {
    char type;
    int32_t pid;
    int32_t sk;
} pgmsg_backend_keydata_t;

typedef struct {
    char type;
    char tr;
} pgmsg_ready_t;

typedef struct {
    char type;
    char *severity;
    char *text;
    char *code;
    char *message;
    char *position;
    char *file;
    char *line;
    char *routine;
    char *detail;
} pgmsg_error_t;

typedef struct {
    const char *fname;
    int32_t oid_table;
    int16_t idx_field;
    int32_t oid_field;
    int16_t field_len;
    int32_t type_mod;
    int16_t field_fmt;
} pgmsg_field_t;

typedef struct {
    char type;
    int16_t nflds;
    pgmsg_field_t fields [0];
} pgmsg_rowdesc_t;

typedef struct {
    int32_t len;
    uint8_t *data;
} pgmsg_data_t;

typedef struct {
    char type;
    int16_t nflds;
    pgmsg_data_t fields [0];
} pgmsg_datarow_t;

typedef struct pgmsg_datarow_item pgmsg_datarow_item_t;
struct pgmsg_datarow_item {
    pgmsg_datarow_item_t *prev;
    pgmsg_datarow_item_t *next;
    pgmsg_datarow_t *datarow;
};

typedef struct {
    size_t len;
    pgmsg_datarow_item_t *head;
} pgmsg_datarow_list_t;
void pgmsg_datarow_list_add (pgmsg_datarow_list_t *list, pgmsg_datarow_t *datarow);
void pgmsg_datarow_list_clear (pgmsg_datarow_list_t *list);

typedef struct {
    char type;
    const char *tag;
} pgmsg_cmd_complete_t;

typedef struct {
    int8_t fmt;
    int16_t cols;
    int16_t fmtcol;
} pgmsg_copyin_t;

typedef union {
    char type;
    pgmsg_auth_t msg_auth;
    pgmsg_param_status_t msg_param_status;
    pgmsg_backend_keydata_t msg_backend_keydata;
    pgmsg_ready_t msg_ready;
    pgmsg_error_t msg_error;
    pgmsg_rowdesc_t msg_rowdesc;
    pgmsg_datarow_t msg_datarow;
    pgmsg_cmd_complete_t msg_complete;
    pgmsg_copyin_t msg_copyin;
} pgmsg_resp_t;

extern char*(*on_pgauth) (const char *prompt, int is_echo);

typedef void* (*parse_param_h) (void*, const char*, const char*, uint32_t*);
void *parse_conninfo (void *data, const char *conn_info, parse_param_h fn, uint32_t *flags);
int pgmsg_set_param (pgmsg_t **msg, const char *name, size_t name_len, const char *value, size_t value_len);

pgmsg_t *pgmsg_create (char type);
pgmsg_t *pgmsg_create_startup (const char *user, size_t user_len, const char *database, size_t database_len);
pgmsg_t *pgmsg_create_startup_params (const char *conn_info);
pgmsg_t *pgmsg_create_pass (int req, const char *salt, size_t salt_len, const char *user, const char *pass);
pgmsg_t *pgmsg_create_sasl_init (conninfo_t *cinfo);
pgmsg_t *pgmsg_create_sasl_fin (pgmsg_resp_t *resp, conninfo_t *cinfo);
pgmsg_t *pgmsg_create_simple_query (const char *sql, size_t sql_len);
pgmsg_t *pgmsg_create_parse (const char *name, size_t name_len, const char *sql, size_t sql_len, int fld_len, pgfld_t **flds);
pgmsg_t *pgmsg_create_bind (const char *portal, size_t portal_len, const char *stmt, size_t stmt_len,
                            int fld_len, pgfld_t **flds, int res_fmt_len, int *res_fmt);
pgmsg_t *pgmsg_create_describe (uint8_t op, const char *name, size_t name_len);
pgmsg_t *pgmsg_create_execute (const char *portal, size_t portal_len, int32_t max_rows);
pgmsg_t *pgmsg_create_close(char what, const char *str, size_t slen);
static inline pgmsg_t *pgmsg_create_sync () { return pgmsg_create(PG_SYNC); };
static inline pgmsg_t *pgmsg_create_term () { return pgmsg_create(PG_TERM); };
pgmsg_t *pgmsg_copyin_flds (int len, pgfld_t **flds);

int pgmsg_send (int fd, pgmsg_t *msg);
int pgmsg_recv (int fd, pgmsg_t **msg);

pgmsg_resp_t *pgmsg_parse (pgmsg_t *msg);

#endif
